EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 7805 U1
U 1 1 5AFFC2BB
P 4300 2900
F 0 "U1" H 4450 2704 50  0000 C CNN
F 1 "7805" H 4300 3100 50  0000 C CNN
F 2 "" H 4300 2900 50  0001 C CNN
F 3 "" H 4300 2900 50  0001 C CNN
	1    4300 2900
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5AFFC306
P 3550 3000
F 0 "C1" H 3575 3100 50  0000 L CNN
F 1 "C" H 3575 2900 50  0000 L CNN
F 2 "" H 3588 2850 50  0001 C CNN
F 3 "" H 3550 3000 50  0001 C CNN
	1    3550 3000
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5AFFC3B5
P 5000 3000
F 0 "C2" H 5025 3100 50  0000 L CNN
F 1 "C" H 5025 2900 50  0000 L CNN
F 2 "" H 5038 2850 50  0001 C CNN
F 3 "" H 5000 3000 50  0001 C CNN
	1    5000 3000
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 5AFFC3D9
P 3100 2850
F 0 "D1" H 3100 2950 50  0000 C CNN
F 1 "D" H 3100 2750 50  0000 C CNN
F 2 "" H 3100 2850 50  0001 C CNN
F 3 "" H 3100 2850 50  0001 C CNN
	1    3100 2850
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 J1
U 1 1 5AFFC5DB
P 2450 2900
F 0 "J1" H 2450 3050 50  0000 C CNN
F 1 "CONN_01X02" V 2550 2900 50  0000 C CNN
F 2 "" H 2450 2900 50  0001 C CNN
F 3 "" H 2450 2900 50  0001 C CNN
	1    2450 2900
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 J2
U 1 1 5AFFC665
P 5600 2900
F 0 "J2" H 5600 3050 50  0000 C CNN
F 1 "CONN_01X02" V 5700 2900 50  0000 C CNN
F 2 "" H 5600 2900 50  0001 C CNN
F 3 "" H 5600 2900 50  0001 C CNN
	1    5600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2850 2950 2850
Wire Wire Line
	3250 2850 3900 2850
Connection ~ 3550 2850
Wire Wire Line
	4700 2850 5400 2850
Connection ~ 5000 2850
Wire Wire Line
	5400 2950 5250 2950
Wire Wire Line
	5250 2950 5250 3150
Wire Wire Line
	5250 3150 3300 3150
Connection ~ 5000 3150
Connection ~ 4300 3150
Wire Wire Line
	3300 3150 3300 2950
Wire Wire Line
	3300 2950 2650 2950
Connection ~ 3550 3150
$EndSCHEMATC
