# DJI F550 hexacopter

We use the DJI F550 hexacopter to test the Aeroteam software and perform missions.

## Prop guard

To avoid broken props we use the F550 3D printed prop guards from [Thingiverse](https://www.thingiverse.com/thing:538607).

## Camera

For a better performance and stability we use a Raspberry Pi 3B with the official Raspberry Pi Camera V2.1 module as videostream.
We mount both things using 3D printed holders from Thingiverse:

- [Camera](https://www.thingiverse.com/thing:1707484)
- [Raspberry Pi](https://www.thingiverse.com/thing:2229982)

Using the UV4L driver we can get a video stream from the Raspberry Pi camera with almost no lag. You can find the installation manual for UV4L [here](http://blog.cudmore.io/post/2016/06/05/uv4l-on-Raspberry-Pi/).
With the following command you can start the camera stream on HTTP port 8080:

`uv4l --driver raspicam --auto-video_nr --encoding h264 --width 640 --height 480 --enable-server on`

The Raspberry Pi is connected over WiFi with the ground station using a hotspot.
