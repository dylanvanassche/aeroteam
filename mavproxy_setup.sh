################################################################################
#                                                                              #
#   MAVProxy configuration for the Aeroteam software setup                     #
#                                                                              #
#   Requirements:                                                              #
#       - MAVProxy installed                                                   #
#       - Python 2.7+                                                          #
#       - Drone connection on /dev/ttyUSB0                                     #
#                                                                              #
#   UDP ports:                                                                 #
#       - QGroundControl: 127.0.0.1:14550                                      #
#       - DroneKit: 127.0.0.1:14551                                            #
#                                                                              #
################################################################################

# Start the MAVProxy server on /dev/ttyUSB0 and make 2 UDP interfaces available
mavproxy.py --master=/dev/ttyUSB0 --out=127.0.0.1:14550 --out=127.0.0.1:14551
