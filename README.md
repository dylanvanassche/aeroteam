# Aeroteam

[![build status](https://gitlab.com/dylanvanassche/aeroteam/badges/master/pipeline.svg)](https://gitlab.com/dylanvanassche/aeroteam/commits/master)

[![coverage report](https://gitlab.com/dylanvanassche/aeroteam/badges/master/coverage.svg)](https://gitlab.com/dylanvanassche/aeroteam/commits/master)

This is the official Aeroteam (KU Leuven) repository for the UAV Challenge by Dassault with all the code and documentation written by the students from Campus De Nayer (KU Leuven) for their bachelor thesis.

The Aeroteam is divided in 2 parts:
- __Campus De Nayer__: writes the software for the autonomous drone
- __Campus Brugge__: deals with the hardware of the drone

## Hardware instructions

### Requirements
- EAVISE drone (DJI F550) from Dries Hulens
- Schematic of the drone (see `./drone`)
- All missing parts (see next paragraph)
- LiPo charger for at least a 4S LiPo battery (14.4 V)
- Laptop with QGroundControl installed, you can get it [here](http://qgroundcontrol.com/) for Linux
- A lot of patience

### Missing parts
First of all, check if the schematic completely matches the EAVISE drone. If not, add all the needed components!

Below we have a list of all the components we added to the drone and how it was done. 
In case something is missing, Google is your best friend to get the data sheet or any information about a certain component.

#### Pixhawk 2
The most important part of the drone is the flight controller: the Pixhawk 2.
The Pixhawk 2 controls the motors, communicates with the ground station, RC control, GPS, ...
The RGB LED on the Pixhawk 2 indicates the status of the drone (armed, GPS, ...), you can find
a list of colors and their meaning [here](https://pixhawk.org/users/status_leds).

All the other parts mentioned below are attached to the Pixhawk 2.

:warning: The arrow on the Pixhawk 2 must point to the front side of the drone.

![Pixhawk 2](./drone/Pixhawk.png "Pixhawk 2")

#### 433 Mhz TX/RX system
To allow communication between the ground station and the drone we need a TX/RX communication system.
On the EAVISE drone we use the 433 Mhz TX/RX modules:

![433 Mhz TX/RX](./drone/433.jpg "433 Mhz TX/RX modules")

##### Installation

1. Plug the USB module in the ground station.
2. Connect the wire bundle to one of the `TELEM` ports of the Pixhawk 2.

#### 3DR GPS
The drone needs to have the option to orient itself to the desired location,
our drone uses the 3DR GPS with external compass to accomplish that goal.

:warning: Do not mount the GPS to close to the power rail of the drone. This might disturb the compass and lead to unstable autonomous flights!

![3DR GPS](./drone/gps.jpg "3DR GPS")

##### Installation

1. Mount the GPS far away from the power rail using zip ties. Pay attention to the _front_ icon!
2. Connect the GPS signal wires to the Pixhawk 2 GPS port.
3. Connect the compass signal wires to the Pixhawk 2 I2C port.

#### 5.8 Ghz streaming with FPV camera
The video streaming kit contains of several components:

- 5.8 Ghz transmitter (FT951) for the drone
- Analog FPV HD camera (Sony PAL)
- 5.8 Ghz receiver for the ground station (external 12 V power supply needed)

:warning: The FPV camera runs on 5 V while the transmitter (6 V - 30 V) can run directly from the LiPo battery.
This is circumvented by using a 7805 linear regulator, the diode in the schematic avoids reverse polarity.

![FPV camera](./drone/camera.JPG "FPV camera")
![FT951](./drone/ft951.jpg "FT951")
![7805](./drone/7805.png "7805")

##### Installation

1. Create a 7805 power supply to power the camera.
2. Attach the video signal (yellow wire) from the camera to the transmitter.
3. Connect the camera to the 7805 power supply and attach the LiPo battery to the 7805.
4. Use zip ties to mount the camera on the bottom of the drone.
5. Add some double-sided tape on the transmitter and mount it.
6. Select the _right_ channel on the transmitter and receiver!

#### 2.4 Ghz manual control RC
The manual control kit exists of several components:

- 2.4 Ghz receiver for the drone
- 2.4 Ghz remote control which allows the user to fly manually

![RC kit](./drone/rc.jpg "RC kit")

##### Installation

1. Connect the receiver to one of the `TELEM` ports of the Pixhawk 2.
2. Select the right channel on the RC and receiver (see the manual of the kit). This might already be done by the previous users (green LED lights up instead of red on the receiver)!

#### Safety parts
Avoiding crashes and accidents are very important when flying with a drone. The
Pixhawk 2 includes several safeguards to avoid those incidents:

- Buzzer to indicate low battery, bad GPS signal, arming, ...
- Arming button, before arming is possible, this button must be pressed manually on the drone.

![Buzzer](./drone/buzzer.png "Buzzer")
![Arming button](./drone/switch.jpg "Arming button")

##### Installation
1. Connect the buzzer to the Pixhawk's `BUZZER` port.
2. Connect the switch to the Pixhawk's `SWITCH` port.

#### ESC & motors
The ESC's provide power to the motors and allow the Pixhawk to control the brushless motors using a servo signal. The ESC generates a 3 phase signal to control the motor.
A servo signal is a square wave with a period between 1.0 ms and 2.0 ms. When we varied the period you get a servo angle between 0 degrees and 180 degrees, in our case 0% to 100% motor power.

![ESC](./drone/E300.jpg "ESC")
![Brushless motor](./drone/motor.jpg "Brushless motor")

##### Installation
1. Connect the 3 motor wires to the ESC output, make sure the order of the wires is the same for every motor.
2. Connect the servo signal input to the Pixhawk 2 main output rail.

#### Power module
The power module is placed between the LiPo battery and the Pixhawk 2. 
It allows the Pixhawk 2 to monitor the current and voltage of the battery while being powered from it (5 V generated by the power module).

![Power module](./drone/powermodule.png "Power module")

##### Installation
1. Connect the power module to the Pixhawk's `POWER` port.
2. Connect all the other consumers to the power module output.
3. Attach the LiPo battery to the power module input.

## Calibration & updates
To function correctly, the hardware and software must be calibrated using the QGroundControl software.
You can find more information about the calibration and firmware updates for the Pixhawk 2 below:

- [RC radio](https://docs.qgroundcontrol.com/en/SetupView/Radio.html)
- [ESC's](https://docs.px4.io/en/advanced_config/esc_calibration.html)
- [Firmware](https://docs.qgroundcontrol.com/en/SetupView/Firmware.html)
- [Drone](https://docs.qgroundcontrol.com/en/SetupView/SetupView.html)

## Flight preparations
To perform a succesful autonomous flight using QGroundControl the following actions are required:

1. Check the drone hardware & software if everything is operational.
2. Check if the laptop is installed with the right software and has enough battery.
3. Connect in QGroundControl to the drone, check the manual of the QGroundControl for more information.
4. Keep the RC control with the kill switch within reach.

## Build instructions

### Requirements and dependencies
- Python 2.7
- Linux computer, preferable OpenSUSE Leap
- PIP installed (if you haven't installed PIP yet, you can download it [here](https://pip.pypa.io/en/stable/installing/) or use your favourite Linux package manager)
- Git installed (use you favourite Linux package manager)
- Docker installed (use you favourite Linux package manager)

You have to clone the whole repository first before you can continue. Cloning can be done with this command:

`git clone https://gitlab.com/dylanvanassche/aeroteam.git`

Enter the project directory and install all the needed Python modules using PIP:

`pip2 install -r requirements.txt`

### Running the project
Since we use Python, nothing needs to be compiled! You can just run the whole
project by navigating to `./src` and run the following command:

`python2 controller.py`

As soon as you entered the connection parameters, you can surf to `http://localhost:7777` where the GUI will be waiting for you!

### Running the tests
As described in the paragraph above, you can just run the tests as a normal project.
Navigate to `./src/tests` and run the following command:

- `python2 -m unittest opencv`
- `python2 -m unittest drone`
- `python2 -m unittest mvc`
- `python2 -m unittest system`
- `python2 -m unittest tools`

Coverage reports can be generated using:

- `coverage2 erase`
- `coverage2 run -a -m unittest opencv`
- `coverage2 run -a -m unittest drone`
- `coverage2 run -a -m unittest mvc`
- `coverage2 run -a -m unittest system`
- `coverage2 run -a -m unittest tools`
- `coverage2 report -m --include=/builds*`

:warning: The include directory can change depending on your configuration! Check the coverage2 documentation for more information. Without this argument, all the coverage reports from the 3rd party modules are also visible.

### Building the Docker image

All tests on this repo are running under Gitlab CI/CD which requires a Docker image
as test environment. A default Docker image from Dockerhub is too slow for our purpose
since we need to install several modules and libraries on ever test run.

We created are own Docker image and uploaded it to the Gitlab registry of our repo.
The Docker image is based on OpenSUSE Leap and it's Dockerfile can be found under `./src/tests/docker` and can be builded using the following command (name = aeroteam):

`docker build -t aeroteam .`


### Building the documentation

All the code in the aeroteam repository has been documented using the builtin
Python documentation tools. To build the documentation run the following commands:

- `cd ./src` 
- `export PYTHONPATH=${PYTHONPATH}:src`
- `pdoc aeroteam --overwrite --html --html-dir docs`

The documentation can be downloaded from the Gitlab CI/CD pipeline as an artificat in the latest step.
The documentation is also available under the following URL: [https://dylanvanassche.gitlab.io/aeroteam-docs/](https://dylanvanassche.gitlab.io/aeroteam-docs/).

Currently, the documentation isn't automatically uploaded to the URL, check the [aeroteam-docs](https://gitlab.com/dylanvanassche/aeroteam-docs) repository for more information.
