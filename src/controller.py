#!/usr/bin/python

import cherrypy
import os
import sys
from aeroteam.gui import Model, View
from aeroteam.models.drone import Drone
from aeroteam.constants import *

"""
The Controller (MVC) provides interaction between the GUI and the backend.
It's also the bootstrapping mechamism to start up all the other components.
"""

__all__ = ["Controller"]

class Controller(object):
    """
    Controller object for Cherrypy with all the available routes and other
    configurations.

    Thanks to [Tutorialspoint.com](https://www.tutorialspoint.com/cherrypy/cherrypy_a_working_application.htm).

    __Parameters__

    - connection_string: Dronekit connection string
    """
    def __init__(self, drone_connection, camera_connection):
        self.root = os.path.join(os.path.abspath(os.path.dirname(__file__)), "aeroteam", "gui")
        self._server_conf = {
            "server.socket_port": 7777,
            "tools.encode.on": True,
            "tools.encode.encoding": "utf-8",
            "tools.decode.on": True,
            "tools.trailing_slash.on": True,
            "tools.staticdir.root": self.root, # Cherrypy allows ONLY absolute paths
            "log.screen": False # Disable in production
        }

        self._assets_conf = {
            "/images": {
                "tools.staticdir.on": True,
                "tools.staticdir.dir": "./images"
            },
            "/css": {
               "tools.staticdir.on": True,
               "tools.staticdir.dir": "./css"
           },
           "/js": {
              "tools.staticdir.on": True,
              "tools.staticdir.dir": "./js"
           },
           "/sounds": {
              "tools.staticdir.on": True,
              "tools.staticdir.dir": "./sounds"
           },
        }

        # Init DroneKit
        self.drone = Drone("AeroDrone", drone_connection)
        self.drone.setup()

        # Init GUI
        self.model = Model(self, self.drone, camera_connection)
        self.view = View(self)
        self.start()

    def get_terminal(self):
        """
        Returns the terminal output from the model to the view.

        __Returns__

        - terminal: the latest terminal output as a Python list of strings
        """
        return self.model.get_terminal()

    def get_position(self):
        """
        Returns the GPS position from the model to the view.

        __Returns__

        - position: Python tuple (latitude, longitude, altitude)
        """
        return self.model.get_position()

    def get_battery(self):
        """
        Returns the battery percentage from the model to the view.

        __Returns__

        - battery: float representing the battery percentage [0.0, 100.0]
        """
        return self.model.get_battery()

    def get_feed(self):
        """
        Returns the current camera feed from the model to the view.

        __Returns__

        - feed: base64 encoded camera frame for directly embedding.
        """
        return self.model.get_feed()

    def get_busy(self):
        """
        Returns the busy state from the model to the view.

        __Returns__

        - busy: boolean state to represent if the drone is executing a mission
        or not.
        """
        return self.model.get_busy()

    def get_detections(self):
        """
        Returns the detections state from the model to the view.

        __Returns__

        - detections: all the detected shapes for the current mission
        """
        return self.model.get_detections()

    def has_bomb(self):
        """
        Returns the bomb state from the model to the view.

        __Returns__

        - has_bomb: Python boolean
        """
        return self.model.has_bomb()

    def mission1(self, startpoint, endpoint, color, shape):
        """
        Starts _mission 1_ in the model after pressing the button in the view.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - endpoint: GPS location of the landing location (LocationGlobalRelative)
        - color: color of the shape as the bomb target (Color enum)
        - shape: Type of shape as the bomb target (Shape)
        """
        self.model.mission1(startpoint, endpoint, color, shape)

    def mission2(self, startpoint, p1):
        """
        Starts _mission 2_ in the model after pressing the button in the view.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - p1: GPS location of the fake waypoint (LocationGlobalRelative)
        """
        self.model.mission2(startpoint, p1)

    def mission3(self, startpoint, p1, p2, p3, p4, color, shape):
        """
        Starts _mission 3_ in the model after pressing the button in the view.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - p1: left-bottom GPS location of the search area (LocationGlobalRelative)
        - p2: left-top GPS location of the search area (LocationGlobalRelative)
        - p3: right-top GPS location of the search area (LocationGlobalRelative)
        - p4: right-bottom GPS location of the search area (LocationGlobalRelative)
        - color: color of the shape as the bomb target (Color enum)
        - shape: Type of shape as the bomb target (Shape)
        """
        self.model.mission3(startpoint, p1, p2, p3, p4, color, shape)

    def tests(self, startpoint, endpoint, p1, p2, p3, p4, shape, color):
        """
        Starts _tests_ in the model after pressing the button in the view.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - endpoint: GPS location of the landing location (LocationGlobalRelative)
        - p1: left-bottom GPS location of the search area (LocationGlobalRelative)
        - p2: left-top GPS location of the search area (LocationGlobalRelative)
        - p3: right-top GPS location of the search area (LocationGlobalRelative)
        - p4: right-bottom GPS location of the search area (LocationGlobalRelative)
        - color: color of the shape as the bomb target (Color enum)
        - shape: Type of shape as the bomb target (Shape)

        __Raises__

        - BusyException: only 1 mission at the same time can be executed by the
        drone

        """
        self.model.tests(startpoint, endpoint, p1, p2, p3, p4, shape, color)

    def abort(self):
        """
        Aborts the current mission in the model.
        """
        self.model.abort()

    def start(self):
        """
        Starts the Cherrypy HTTP webserver.
        Based on [quickstart()](https://github.com/cherrypy/cherrypy/blob/master/cherrypy/__init__.py#L154)
        but without the blocking component.
        This method also connects to the DroneKit drone.
        """
        # Configure the server
        cherrypy.config.update(self._server_conf)

        # Configure the application
        cherrypy.tree.mount(self.view, "/", config=self._assets_conf)

        # Launch
        cherrypy.engine.signals.subscribe()
        cherrypy.engine.start()

    def stop(self):
        """
        Stops the Cherrypy webserver and all other running threads.
        """
        cherrypy.engine.exit()
        self.model.stop()

# Launch the Controller
if __name__ == "__main__": # pragma: no cover
    drone_connection = raw_input("Drone connection: ")
    camera_connection = raw_input("Camera connection: ")
    c = Controller(drone_connection, camera_connection)
    c.start()
    print("Connected to: {0} with camera {1}".format(drone_connection, camera_connection))
