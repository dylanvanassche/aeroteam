# -*- coding: utf-8 -*-
#RENAME TO Tools.py BEFORE PUSHING

import unittest
import sys
sys.path.append("../")
from aeroteam.tools import *

"""
@author: Wout
"""
class TestTools(unittest.TestCase):
    def print_header(self, text):
        """
        Prints a header with the name of the test.
        """
        HEADER_LENGTH = 80
        fill = int(round((HEADER_LENGTH - len(text))/2.0))
        print("\n" + "-"*fill + text + "-"*fill)
        
        
    def test_GPScalc(self):
        """
        Tests the internal representations of GPS coordinates and waypoints.
        """
        self.print_header("TEST GPS CALCULATIONS")
        
        #Test the internal representation of GPS coordinates
        c1 = Tools.waypoint(51.100669, 4.497718)
        self.assertEqual(c1.lat, 51.100669)
        self.assertEqual(c1.lon, 4.497718)
        self.assertEqual(c1.alt, 0.0)
        print c1
        print "Checked against (51.100669, 4.497718, 0.0)"
        print
        
        #Test the internal representation of a waypoint
        w1 = Tools.waypoint(51.101439, 4.452625, 4.0, "Home of the Brave")
        self.assertEqual(w1.name, "Home of the Brave")
        self.assertEqual(w1.gps.lat, 51.101439)
        self.assertEqual(w1.gps.lon, 4.452625)
        self.assertEqual(w1.gps.alt, 4.0)
        print w1
        print "Checked against (Home of the Brave, 51.101439, 4.452625, 4.0)"
        print
        print
        
        
    def test_intersect(self):
        """
        Tests the intersect algorithm that is making use of the gradient method,
        the method that creates a Shapely LineString from 2 points, and the 
        method that creates the equation of a line out of a Shapely LineString.
        Also uses the wrapper method for the LineString from 2 Points.
        """
        self.print_header("TEST INTERSECT ALGORITHM")
        
        #Test the LineStrings created with 2 points
        p1 = Tools.waypoint(1,3)
        self.assertEqual(p1.lat, 1)
        self.assertEqual(p1.lon, 3)
        self.assertEqual(p1.alt, 0.0)
        p2 = Tools.waypoint(-2,5)
        self.assertEqual(p2.lat, -2)
        self.assertEqual(p2.lon, 5)
        self.assertEqual(p2.alt, 0.0)
        laurens = Tools.lineString2P(p1, p2)
        self.assertEqual(laurens.coords[0][0], 1)
        self.assertEqual(laurens.coords[0][1], 3)
        self.assertEqual(laurens.coords[1][0], -2)
        self.assertEqual(laurens.coords[1][1], 5)
        print laurens
        print "Checked against ((1, 3), (-2, 5))"
        print
        
        p3 = Point(0,0)
        p4 = Point(1,1)
        dylan = Tools.lineString2P(p3, p4)
        self.assertEqual(dylan.coords[0][0], 0)
        self.assertEqual(dylan.coords[0][1], 0)
        self.assertEqual(dylan.coords[1][0], 1)
        self.assertEqual(dylan.coords[1][1], 1)
        print dylan
        print "Checked against ((0, 0), (1, 1))"
        print
        print
        
        #Test the intersection algorithm
        p5 = Tools.findIntersect(laurens, dylan)        
        self.assertEqual(p5.x, 2.2)
        self.assertEqual(p5.y, 2.2)
        self.assertEqual(p5.alt, 0.0)
        print "Intersection:", p5
        print "Checked against (2.2, 2.2, 0.0)"
        print
        print
        
        
    def test_Img2GPS(self):
        """
        Tests the algorithm to convert IMAGE COORDINATES to GPS COORDINATES.
        """
        self.print_header("TEST IMG2GPS ALGORITHM")
        
        #Test with an altitude that is too low, should throw an exception
        try:
            c1 = Tools.waypoint(51.100669, 4.497718, 1.0)
            Tools.img2GPS(Point(0, 1), c1, 1200, 600, 45)
            self.assertTrue(False)
        except LowAltitudeException as e:
            print "Altitude should be too low for calculations:"
            print e
            print
            print
        
        #Test with an altitude that's high enough, should not throw an exception
        try:
            c1 = Tools.waypoint(51.100669, 4.497718, 3.0)
            ptGPS = Tools.img2GPS(Point(0, 1), c1, 1200, 600, 45)
            self.assertEqual(ptGPS.lat, 51.100685503235219)
            self.assertEqual(ptGPS.lon, 4.4977092006975372)
            self.assertEqual(ptGPS.alt, 3.0)
            print "GPS coordinate from Image coordinate:", ptGPS
            print "(Beware: values in print statement are rounded to 12 digits)"
            print "Checked against (51.10068130626793, 4.4977375974122467, 3.0)"
            print
            print
            
        except LowAltitudeException as e:
            print e
            print
            print
            self.assertTrue(False)
            
    def test_scanPattern(self):
        """
        Tests the algorithm to calculate the GPS coordinates of the scan pattern.
        """
        self.print_header("TEST SCANPATTERN ALGORITHM")
        
        p1 = Tools.waypoint(0, 0)
        p2 = Tools.waypoint(0, 15)
        p3 = Tools.waypoint(15, 15)
        p4 = Tools.waypoint(15, 0)
        FIELDSIDE = 15.0        #The field that has to be scanned is 15m x 15m
        CENTREBAND = 1.2        #Camera only looks at the inner 1.2m x 1.2m of the image
        lijst = Tools.scanPattern(p1, p2, p3, p4)
        
        self.assertEqual(lijst[0].lat, 0.0)
        self.assertEqual(lijst[0].lon, 0.0)
        print "GPS coordinate within the scanPattern at position 0:", lijst[0]
        print "Checked against (0.0, 0.0)"
        print
        
        self.assertEqual(lijst[73].lat, 6.0)
        self.assertEqual(lijst[73].lon, 12.0)
        print "GPS coordinate within the scanPattern at position 73:", lijst[73]
        print "Checked against (6.0, 12.0)"
        print
        
        self.assertEqual(lijst[144].lat, 12.0)
        self.assertEqual(lijst[144].lon, 4.8)
        print "GPS coordinate within the scanPattern at position 144:", lijst[144]
        print "Checked against (12.0, 4.8)"
        print
        
        self.assertEqual(lijst[168].lat, 14.4)
        self.assertEqual(lijst[168].lon, 0.0)
        print "GPS coordinate within the scanPattern at position 168:", lijst[168]
        print "Checked against (14.4, 0.0)"
        print
        
        self.assertEqual(lijst[195].lat, 15.6)
        self.assertEqual(lijst[195].lon, 0.0)
        print "GPS coordinate within the scanPattern at position 195:", lijst[195]
        print "Checked against (15.6, 0.0)"
        print

        
# Start unittest
if __name__ == '__main__':
    unittest.main()
