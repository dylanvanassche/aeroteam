import unittest
import sys
sys.path.append("../")
import Queue
import cv2
import dronekit_sitl
from aeroteam.models.drone import Drone
from aeroteam.fetcher import Fetcher
from aeroteam.detector import Detector, UnknownShapeException
from aeroteam.filtering import Filter
from aeroteam.constants import *
from aeroteam.processor import Processor
from aeroteam.models.colors import UnknownColorException, Color
from aeroteam.models.shapes import InvalidShapeException, Rectangle, Cross, Arrow

CAM_RESOURCE = "./missions/Mission3_low.mp4"

frame_list = ["arrow1_forwards", "arrow2_forwards", "arrow3_forwards", \
"arrow4_forwards", "arrow5_backwards", "arrow6_backwards_angle", \
"cross1", "rectangle2", "rectangle3", "cross2", "cross_low_light", \
"invalid_shape", "invalid_color_rectangle", "unknown"]

class TestOpenCV(unittest.TestCase):

    def read_contours_from_img(self, frame_name, scale=BOUNDRY_BOX_UPSCALE):
        """
        Helper method to read the contours of the given frame.
        This method will save it's results as images in the current working
        directory.

        The frame is scaled for better performance.
        """
        f = Filter()
        d = Detector()
        frame = cv2.imread(frame_name + ".jpg")
        frame = cv2.resize(frame, (0,0), fx=scale, fy=scale)
        frame = f.bilateral(frame)
        frame, color = f.remove_grass(frame)
        cv2.imwrite("./tmp/" + frame_name + "_filtered.jpg", frame)
        contours = d.find_contours(frame)
        frame = d.draw_contours(contours, frame, GREEN, LINE_SIZE)
        cv2.imwrite("./tmp/" + frame_name + "_contoured.jpg", frame)
        return contours, frame, color

    def print_header(self, text):
        """
        Prints a header with the name of the test.
        """
        HEADER_LENGTH = 80
        fill = int(round((HEADER_LENGTH - len(text))/2.0))
        print("\n" + "-"*fill + text + "-"*fill)

    def test_init(self):
        """
        Tries to create a Detector and Filter instance.
        """
        self.print_header("TEST INIT")
        d = Detector()
        f = Filter()
        print(d)
        print(f)

    def test_filter_contours(self):
        """
        Tries to filter the image and read it's contours.
        """
        self.print_header("TEST FILTER CONTOURS")

        # Read contours for each frame
        for frame_name in frame_list:
            # Override scaling due small images
            contours, frame, color = self.read_contours_from_img(frame_name, 2.0)
        # Checking for valid contours is done in the other tests

    def test_convert_to_shape(self):
        """
        Tries to convert the contours of an image into shapes.
        """
        self.print_header("TEST COVERT TO SHAPE")

        d = Detector()

        for frame_name in frame_list:
            # Read contours, override scaling due small images
            contours, frame, color = self.read_contours_from_img(frame_name, 2.0)

            # Check if the shape matches the given input
            try:
                print("\nFrame: {0}".format(frame_name))

                # Skip detection if color is unknown
                if(color == Color.UNKNOWN):
                    self.assertTrue("invalid_color" in frame_name or "unknown" in frame_name or "light" in frame_name)
                    continue

                shape = d.find_shape(contours, frame, color)
                print("Shape: {0}".format(shape))

                if isinstance(shape, Rectangle):
                    self.assertTrue("rectangle" in frame_name)
                    shape.find_center()

                elif isinstance(shape, Arrow):
                    self.assertTrue("arrow" in frame_name)

                    # When find_direction() fails to calculate the angle and
                    # direction, an InvalidShapeException is raised.
                    angle, tip, back, center = shape.find_direction()
                    print("Points (tip|back|center): {0}|{1}|{2} Angle: {3} degrees".format(tip["point"], back["point"], center, angle))

                    magnitude = 0
                    GRAY_AREA_ANGLE = 2.0 # avoid miscalculations
                    if(0.0 <= angle < 180.0-GRAY_AREA_ANGLE):
                        magnitude = 1
                    elif(180.0+GRAY_AREA_ANGLE <= angle < 360.0):
                        magnitude = -1

                    if(magnitude == 1):
                        self.assertTrue("forwards" in frame_name)
                    elif(magnitude == -1):
                        self.assertTrue("backwards" in frame_name)
                    elif(magnitude == 0):
                        self.assertTrue("angle" in frame_name)

                elif isinstance(shape, Cross):
                    self.assertTrue("cross" in frame_name)
                    shape.find_center()

                # Draw results of the shape detection on the frame
                frame = d.draw_contours([shape.contours], frame, BLUE, LINE_SIZE)
                frame = d.draw_contours(shape.contours, frame, RED, POINT_SIZE)
                cv2.imwrite("./tmp/" + frame_name + "_contoured_aprox.jpg", frame)

            except UnknownShapeException:
                print("Unknown shape, automatically invalid")
                self.assertTrue("unknown" in frame_name or "invalid" in frame_name)
            except InvalidShapeException:
                print("Invalid shape or brightness issue")
                self.assertTrue("invalid_shape" in frame_name or "high_light" in frame_name or "low_light" in frame_name)

    def test_stream(self):
        """
        Tries to read a video stream and perform some detections on it.
        This test comes very close to the missions unittests.
        """
        self.print_header("TEST STREAM COMPONENTS")
        c = Fetcher(CAM_RESOURCE)
        d = Detector()
        f = Filter()

        frame_counter = 0
        detected_shapes = []
        detector_buffer = []
        while True:
            # Get the frame
            try:
                frame = c.get_frame(timeout=10)
            except Queue.Empty:
                break # end of video stream

            # Process the frame
            frame = cv2.resize(frame, (0,0), fx=BOUNDRY_BOX_UPSCALE, fy=BOUNDRY_BOX_UPSCALE)
            frame = f.bilateral(frame)
            frame, color = f.remove_grass(frame)
            contours = d.find_contours(frame)
            frame = d.draw_contours(contours, frame, GREEN, LINE_SIZE)
            frame_counter = frame_counter + 1

            try:
                shape = d.find_shape(contours, frame, color)
                #print("Shape: {0}, frame: {1}".format(shape, frame_counter))

                if shape not in detected_shapes:
                    # Arrow needs extra calculation
                    if isinstance(shape, Arrow):
                        # When find_direction() fails to calculate the angle and
                        # direction, an InvalidShapeException is raised.
                        angle, tip, back, center = shape.find_direction()
                        #print("Points (tip|back|center): {0}|{1}|{2} Angle: {3} degrees".format(tip["point"], back["point"], center, angle))
                    else:
                        center = shape.find_center()

                    # Only use the frames when the object is in the middle of the camera
                    # Detecting a cross at the borders of the camera can lead to incorrect detections.
                    # The cross will be seen as an arrow (half covered).
                    height, width, channels = frame.shape
                    centerXMin = (width/2)*0.5
                    centerXMax = (width/2)*1.5
                    centerYMin = (height/2)*0.5
                    centerYMax = (height/2)*1.5

                    # Check if we were tracking this shape already and that the shape isn't in the detected_shapes already
                    if (centerXMin < center.x < centerXMax) and (centerYMin < center.y < centerYMax):
                        if len(detector_buffer) == 0 or shape in detector_buffer:
                            detector_buffer.append(shape)
                            if(len(detector_buffer) >= 3):
                                print("Confirmed shape: {0}".format(shape))
                                detected_shapes.append(shape)
                                detector_buffer = []
                                frame = d.draw_contours([shape.contours], frame, BLUE, LINE_SIZE)
                                frame = d.draw_contours(shape.contours, frame, RED, POINT_SIZE)
                                cv2.imwrite("./tmp/" + "stream{0}_contoured_aprox.jpg".format(frame_counter), frame)
            except Exception as e:
                pass

        print("# Detected shapes: {0}".format(len(detected_shapes)))
        self.assertTrue(len(detected_shapes) > 0)

    def test_processor(self):
        """
        Tries to run the Processor class which implements the `test_stream` from
        above as a proper seperate thread.
        """
        self.print_header("TEST PROCESSOR")

        # Create DroneKit SITL simulator
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()
        drone = Drone("AeroDrone", connection_string)
        drone.setup()
        drone.arm(True)
        drone.set_mode("GUIDED")
        drone.set_airspeed(DEFAULT_AIRSPEED)
        drone.set_groundspeed(DEFAULT_GROUNDSPEED)
        drone.take_off(DEFAULT_ALTITUDE) # Sets the home altitude

        # Create a Processor worker
        p = Processor(CAM_RESOURCE, drone)
        d = []

        # Fetch the detections
        while(p.is_running):
            try:
                d = p.get_detections(timeout=20)
            except Queue.Empty:
                break

        # Stop workers
        p.stop()
        drone.stop()
        sitl.stop()

        print("# Detected shapes: {0}".format(len(d)))
        self.assertTrue(len(d) > 0)

# Start unittest
if __name__ == "__main__": # pragma: no cover
    unittest.main()
