import sys
sys.path.append("../")
import unittest
import dronekit_sitl
import os
import cv2
from dronekit import LocationGlobalRelative
from aeroteam.filtering import *
from aeroteam.detector import *
from aeroteam.models import *
from aeroteam.processor import *

STREAM_M3 = os.path.abspath("./missions/Mission3_low.mp4")
img = cv2.imread('arrow1_forwards.jpg')


class TestMission(unittest.TestCase):
    def make_mission3(self):
        """
        Creates dummy drone, dummy filter, dummy detector and libdarknet
        objects.
        """
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()
        drone = Drone("AeroDrone", connection_string)
        m = Mission3(drone, STREAM_M3)
        m.drone.setup()
        return m

    def test_run_mission3(self):
        # startlocation -35.3632604,lon=149.1652281
        m = self.make_mission3()
        p1 = LocationGlobalRelative(-35.3632604, 149.1652281, 30)
        p2 = LocationGlobalRelative(-35.3632604, 149.1652281 + 0.00001, 30)
        p3 = LocationGlobalRelative(-35.3632604 + 0.00001, 149.1652281 + 0.00001, 30)
        p4 = LocationGlobalRelative(-35.3632604 + 0.00001, 149.1652281, 30)
        m.run(p1, p2, p3, p4, Rectangle, Color.RED)

# Start unittest
if __name__ == '__main__': # pragma: no cover
    unittest.main()
