import unittest
import sys
sys.path.append("../")
import dronekit_sitl
import numpy as np
import os
import time
import requests
from aeroteam import *
from controller import Controller
from dronekit import LocationGlobalRelative

URL = "http://localhost:7777"
STREAM = "./missions/Mission1_low.mp4"

class TestSystem(unittest.TestCase):

    def print_header(self, text):
        """
        Prints a header with the name of the test.
        """
        HEADER_LENGTH = 80
        fill = int(round((HEADER_LENGTH - len(text))/2.0))
        print("\n" + "-"*fill + text + "-"*fill)

    def test_controller(self):
        """
        Tries to start the Controller and make everything ready for it's first
        mission.
        """
        self.print_header("TEST CONTROLLER")

        # Create DroneKit SITL simulator
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()

        # Make the drone ready for GUIDED commands
        c = Controller(connection_string, STREAM)

        payload = {
            "point_t_lat": -35.3632606,
            "point_t_lon": 149.1652288,
            "point_l_lat": -35.3633606,
            "point_l_lon": 149.1651288,
            "color":"red",
            "shape":"rectangle"
        }
        mission1 = requests.get(URL + "/mission1", params=payload)
        print("{0}: {1}".format(mission1.url, mission1.status_code))
        print(mission1.text)
        self.assertTrue(mission1.status_code == 200)

        # Stop everything properly
        c.stop()
        sitl.stop()

# Start unittest
if __name__ == "__main__": # pragma: no cover
    unittest.main()
