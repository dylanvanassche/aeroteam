import unittest
import sys
import dronekit_sitl
sys.path.append("../")
from controller import Controller
import requests
import time

URL = "http://localhost:7777"
STREAM = "./missions/Mission1_low.mp4"

class TestGUI(unittest.TestCase):

    def print_header(self, text):
        """
        Prints a header with the name of the test.
        """
        HEADER_LENGTH = 80
        fill = int(round((HEADER_LENGTH - len(text))/2.0))
        print("\n" + "-"*fill + text + "-"*fill)

    def test_api(self):
        """
        Tests the API endpoints.
        """
        self.print_header("TEST API endpoints")

        # Create DroneKit SITL simulator
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()

        # Start the Cherrypy webserver
        c = Controller(connection_string, STREAM)
        c.start()
        print("Connected to: {0} with camera {1}".format(connection_string, STREAM))

        # Index HTML page
        index = requests.get(URL)
        self.assertTrue(index.status_code == 200)
        time.sleep(0.1)

        # Update endpoint with real-time data
        update = requests.get(URL + "/update")
        print("{0}: {1}".format(update.url, update.status_code))
        self.assertTrue(update.status_code == 200)
        time.sleep(0.1)

        # Mission forms data
        payload = [{"endpoint": "mission1"}, {"endpoint": "mission2"}, {"endpoint": "mission3"}, {"endpoint": "tests"}]
        for data in payload:
            mission_form = requests.get(URL + "/mission_form", params=data)
            print("{0}: {1}".format(mission_form.url, mission_form.status_code))
            self.assertTrue(mission_form.status_code == 200)
            time.sleep(0.1)

        # Start mission 1: target
        payload = {
            "point_t_lat": -35.3632606,
            "point_t_lon": 149.1652288,
            "point_l_lat": -35.3633606,
            "point_l_lon": 149.1651288,
            "color":"red",
            "shape":"rectangle"
        }
        mission1 = requests.get(URL + "/mission1", params=payload)
        print("{0}: {1}".format(mission1.url, mission1.status_code))
        print(mission1.text)
        self.assertTrue(mission1.status_code == 200)
        time.sleep(30) # Wait 30s
        abort = requests.get(URL + "/abort")
        print("{0}: {1}".format(abort.url, abort.status_code))
        self.assertTrue(abort.status_code == 200)
        time.sleep(0.1)

        # Start mission 2: intersect
        #payload = {
        #    "point_t_lat": 50.12345,
        #    "point_t_lon": 4.2356,
        #    "point_1_lat": 50.456789,
        #    "point_1_lon": 4.7894
        #}
        #mission2 = requests.get(URL + "/mission2", params=payload)
        #print("{0}: {1}".format(mission2.url, mission2.status_code))
        #self.assertTrue(mission2.status_code == 200)
        #abort = requests.get(URL + "/abort")
        #self.assertTrue(abort.status_code == 200)
        #time.sleep(0.1)

        # Shutdown everything
        try:
            stop = requests.get(URL + "/stop")
            print("{0}: {1}".format(stop.url, stop.status_code))
        except requests.ConnectionError:
            # /stop will close the connection and a connection error will be
            # raised by Python Requests
            print("Webserver shutdown detected succesfully")
            self.assertTrue(True)
        except Exception as e:
            print(e)
            self.assertTrue(False)

# Start unittest
if __name__ == "__main__": # pragma: no cover
    unittest.main()
