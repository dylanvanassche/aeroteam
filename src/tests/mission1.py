import sys
sys.path.append("../")
import unittest
import dronekit_sitl
import os
import cv2
from dronekit import LocationGlobalRelative
from aeroteam.filtering import *
from aeroteam.detector import *
from aeroteam.models import *
from aeroteam.processor import *

STREAM_M1 = os.path.abspath("./missions/Mission1_low.mp4")

img = cv2.imread('arrow1_forwards.jpg')


class TestMission(unittest.TestCase):

    def make_mission1(self):
        """
        Creates dummy drone, dummy filter, dummy detector and libdarknet
        objects.
        """
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()
        drone = Drone("AeroDrone", connection_string)

        m = Mission1(drone, STREAM_M1)
        m.drone.setup()
        return m

#    def test_init(self):
#        m = self.make_mission2()
#        assert isinstance(m, Mission2)


#    def test_save_shape(self):
#        m = self.make_mission2()
#        m.save_shape(img)
#        assert(len(m.shapes) == 1)

#    def test_is_close_enough(self):
#        m = self.make_mission2()
#        m.is_close_enough(img)

#    def test_center(self):
#        m = self.make_mission2()
#        m.center(img)

    def test_run_mission1(self):
        # startlocation -35.3632604,lon=149.1652281


        m = self.make_mission1()

        p1 = LocationGlobalRelative(-35.3632604, 149.1652281, 30)
        p3 = LocationGlobalRelative(-35.3632604 + 0.00001, 149.1652281 + 0.00001, 30)

        m.run(p1, p3, Rectangle, Color.RED)


# Start unittest
if __name__ == '__main__': # pragma: no cover
    unittest.main()
