import unittest
import sys
sys.path.append("../")
import dronekit_sitl
import numpy as np
from dronekit import LocationGlobalRelative
from aeroteam.models.drone import Drone

# Tolerance is needed due the inaccuracy of GPS
DEFAULT_ALTITUDE = 15.0 # 15.0 m
DEFAULT_AIRSPEED = 7.5 # 7.5 m/s
DEFAULT_GROUNDSPEED = 5.0 # 5.0 m/s
DEFAULT_DROP_ALTITUDE = 2.0 # 2.0 m
MAX_TOLLERANCE_POSTION = 0.01 # 1%
MAX_TOLLERANCE_ALTITUDE_FLYING = 1.0 # 1.0 m
MAX_ALTITUDE_LANDING = 0.25 # 0.25 m

class TestDronekit(unittest.TestCase):

    def print_header(self, text):
        """
        Prints a header with the name of the test.
        """
        HEADER_LENGTH = 80
        fill = int(round((HEADER_LENGTH - len(text))/2.0))
        print("\n" + "-"*fill + text + "-"*fill)

    def observer_battery(self, dronekit_self, name, msg):
        """
        Observes the battery state of the drone. This is a callback function and requires 2x a `self` argument!
        The 1st `self` is frome the `TestDroneKit` class, the 2nd is from the Dronekit `Vehicle` object.
        """
        print("Observer {0}: {1}".format(name, msg))

    def test_go_to_and_land(self):
        """
        Tries to execute a simple go to and land mission to test if all critical
        systems are operational. This test also drops the package when flying
        from waypoint 1 to waypoint 2.
        """
        self.print_header("TEST GO TO AND LAND")

        # Create DroneKit SITL simulator
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()

        # Make the drone ready for GUIDED commands
        d = Drone("AeroDrone", connection_string)
        print(d)
        d.setup()
        d.arm(True)
        d.set_mode("GUIDED")
        d.set_airspeed(DEFAULT_AIRSPEED)
        d.set_groundspeed(DEFAULT_GROUNDSPEED)
        d.add_observer("battery", self.observer_battery) # test observer heartbeat
        d.take_off(DEFAULT_ALTITUDE) # Sets the home altitude
        d.remove_observer("battery", self.observer_battery) # Remove it again

        # Get the current heading
        print("Heading: {0} degrees".format(d.get_heading()))

        # Move to waypoint 1 (+- 15 metres)
        current_location = d.get_location()
        target_location = LocationGlobalRelative(current_location.lat + 0.0001, current_location.lon - 0.0001, current_location.alt + 3.0)
        d.go_to_gps(target_location)

        # Drop the package
        d.drop_package(DEFAULT_DROP_ALTITUDE)

        # Move to waypoint 2 (+- 30 metres)
        current_location = d.get_location()
        target_location = LocationGlobalRelative(current_location.lat - 0.0002, current_location.lon + 0.0002, current_location.alt + 2.0)
        d.go_to_gps(target_location)

        # Move to waypoint 3 (+- 5 metres altitude ONLY)
        current_location = d.get_location()
        target_location = LocationGlobalRelative(current_location.lat, current_location.lon, current_location.alt + 5.0)
        d.go_to_gps(target_location)

        # Move to fake waypoint (non blocking go_to_gps)
        current_location = d.get_location()
        target_location = LocationGlobalRelative(current_location.lat - 0.0002, current_location.lon + 0.0002, current_location.alt + 2.0)
        d.go_to_gps(target_location, blocking=False)

        # Move to landing point (+- 47 metres)
        current_location = d.get_location()
        target_location = LocationGlobalRelative(current_location.lat + 0.0003, current_location.lon + 0.0003, current_location.alt - 7.5)
        d.go_to_gps(target_location)

        # Verify that we reached our landing point
        # GPS lat and lon are pretty accurate, altitude is never very accurate
        current_location = d.get_location()
        print(target_location)
        print(current_location)
        self.assertTrue(np.isclose(target_location.lat, current_location.lat, atol=MAX_TOLLERANCE_POSTION))
        self.assertTrue(np.isclose(target_location.lon, current_location.lon, atol=MAX_TOLLERANCE_POSTION))
        self.assertTrue(np.isclose(target_location.alt, current_location.alt, atol=MAX_TOLLERANCE_ALTITUDE_FLYING))

        # Initiate landing procedure
        d.land()
        self.assertTrue(d.get_location().alt <= MAX_ALTITUDE_LANDING)

        # Shutdown the vehicle and the SITL simulator
        d.stop()
        sitl.stop()

# Start unittest
if __name__ == "__main__": # pragma: no cover
    unittest.main()
