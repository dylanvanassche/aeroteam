#!/usr/bin/python

"""
The processor combines several OpenCV based classes into one.
The user can use only the Processor class without knowing the underlying
architecture for processing the video stream.
"""
import threading
import signal
import Queue
import cv2
from aeroteam.models.drone import *
from aeroteam.models.colors import *
from aeroteam.models.shapes import Arrow
from aeroteam.constants import *
from aeroteam.detector import Detector, UnknownShapeException
from aeroteam.filtering import Filter
from aeroteam.fetcher import Fetcher

# Expose only public classes
__all__ = ["Processor"]

class ProcessorThread(threading.Thread):
    """
    Runs the Processor real Thread from filtering to processing the detections.

    __Parameters__

    - queue: Python `Queue.Queue()` buffer for the detections
    - frame_buffer: Python `Queue.Queue()` buffer for the frames
    - fetcher: Fetcher instance
    - filtering: Filter instance
    - detector: Detector instance
    - drone: Drone instance (already setted up)
    """
    def __init__(self, queue, frame_buffer, fetcher, filtering, detector, drone):
        super(ProcessorThread, self).__init__()
        self.daemon = True # Python won't exit until this thread is finished
        self.shutdown = threading.Event()
        self.queue = queue
        self.frame_buffer = frame_buffer
        self.detected_shapes = []
        self.detection_buffer = []
        self.fetcher = fetcher
        self.filter = filtering
        self.detector = detector
        self.drone = drone

    def run(self):
        """
        Executes the Processor Thread itself. This thread does several steps from
        fetching the frame to processing the detections:

        1. Fetch and resize the frame
        2. Filter the frame using a Filter instance
        3. Detect contours, shapes, ... using a Detector instance
        4. Add GPS location to the detections
        """
        while(not(self.shutdown.is_set())):
            try:
                # Get the frame
                frame = self.fetcher.get_frame(timeout=10)

                # Make some space in the frame buffer if needed
                if self.frame_buffer.full():
                    self.frame_buffer.get()

                # Put the new frame in the buffer
                self.frame_buffer.put(frame)
            except Queue.Empty:
                break # end of video stream

            # Process the frame
            # Resize for better performance
            frame = cv2.resize(frame, (0,0), fx=BOUNDRY_BOX_UPSCALE, fy=BOUNDRY_BOX_UPSCALE)

            # Filter the frame
            frame = self.filter.bilateral(frame)
            frame, color = self.filter.remove_grass(frame)

            # Find contours
            contours = self.detector.find_contours(frame)

            try:
                # Find shape
                shape = self.detector.find_shape(contours, frame, color)

                # Check if we haven't found this shape yet
                new_shape = True
                for d in self.detected_shapes:
                    if shape == d["shape"]:
                        new_shape = False
                        break;

                if new_shape:
                    # Arrow needs extra calculation
                    if isinstance(shape, Arrow):
                        # When find_direction() fails to calculate the angle and
                        # direction, an InvalidShapeException is raised.
                        angle, tip, back, center = shape.find_direction()
                    else:
                        center = shape.find_center()

                    # Only use the frames when the object is in the middle of
                    # the camera
                    # Detecting a cross at the borders of the camera can lead
                    # to incorrect detections.
                    # The cross will be seen as an arrow (half covered).
                    height, width, channels = frame.shape
                    centerXMin = (width/2.0)*PROCESSOR_MIN_CENTER
                    centerXMax = (width/2.0)*PROCESSOR_MAX_CENTER
                    centerYMin = (height/2.0)*PROCESSOR_MIN_CENTER
                    centerYMax = (height/2.0)*PROCESSOR_MAX_CENTER

                    # Check if we're in the center interval
                    if (centerXMin < center.x < centerXMax) and (centerYMin < center.y < centerYMax):
                        # Add the shape to the buffer if it's empty or if we're tracking it
                        # If not, skip it
                        if len(self.detection_buffer) == 0 or shape in self.detection_buffer:
                            self.detection_buffer.append(shape)
                            # As soon as we have enough hits we can confirm the shape
                            if(len(self.detection_buffer) >= PROCESSOR_MIN_FRAMES_HIT):
                                print("Confirmed shape: {0}".format(shape))
                                self.detected_shapes.append({"shape": shape, "location": self.drone.get_location()})
                                self.queue.put(self.detected_shapes)
                                self.detection_buffer = []

            #except Exception as e:
            #    pass
            # These exceptions aren't usefull for us
            # They only indicate that the frame is useless (+ reason)
            except (UnknownShapeException, UnknownColorException) as e:
                pass


class Processor(object):
    """
    Avoid using the `Detector`, `Filter` or `Fetcher` classes, the Processor
    class takes care of everything for you. It runs in a seperate thread to
    avoid blocking the main thread.

    __Parameters__

    - cam: path of the camera, for example: `/dev/video0` or `0`
    - drone: Drone instance
    """
    def __init__(self, cam, drone):
        self.is_running = True
        self._detector = Detector()
        self._filter = Filter()
        self._fetcher = Fetcher(cam)
        self._number_of_detections = 0
        self._drone = drone

        # Register signal handlers
        try:
            signal.signal(signal.SIGTERM, self.stop)
            signal.signal(signal.SIGINT, self.stop)
        except ValueError:
            print("Signals not registered since we aren't running in the main thread")

        # Start processing
        self._queue = Queue.Queue(30) # Shape detected buffer
        self._frame_buffer = Queue.Queue(30) # Frame buffer
        self._worker = ProcessorThread(self._queue, self._frame_buffer, self._fetcher, self._filter, self._detector, self._drone)
        self._worker.start()

    def restart(self):
        """
        Starts a new worker thread to fetch new detections.
        This will reset everything in the Processor class, including the old
        worker thread (if running)!
        """
        # Trigger worker shutdown
        self._worker.shutdown.set()
        self.is_running = False

        # Avoid blocking Queue when joining (deadlock)
        # https://stackoverflow.com/questions/3121622/python-multiprocessing-queue-deadlocks-on-put-and-get#3571687
        while(not(self._queue.empty())):
            self._queue.get()

        if self._worker is not None and self._worker.is_alive():
            print("Killing previous worker...")
            self.stop()

        self._queue = Queue.Queue(30) # Shape detected buffer
        self._frame_buffer = Queue.Queue(30) # Frame buffer
        self._worker = ProcessorThread(self._queue, self._frame_buffer, self._fetcher, self._filter, self._detector, self._drone)
        self._worker.start()
        self.is_running = True

    def stop(self, signum=None, frame=None):
        """
        Shutdowns the worker thread.
        """
        # Trigger worker shutdown
        self._worker.shutdown.set()
        self.is_running = False

        # Avoid blocking Queue when joining (deadlock)
        # https://stackoverflow.com/questions/3121622/python-multiprocessing-queue-deadlocks-on-put-and-get#3571687
        while(not(self._queue.empty())):
            self._queue.get()

        # Wait until our worker is joined
        if self._worker.is_alive():
            self._worker.join()
        print("Worker thread ended")

        # Stop fetcher
        self._fetcher.stop()

    def get_detections(self, timeout, block=True):
        """
        Retrieves the list of detections done by OpenCV. The list is updated
        every time a new detections is confirmed. The list will _never_ hold
        duplicate objects.

        __Parameters__

        - timeout: How long this method may block before it throws a
        `Queue.Empty` exception
        - block: When `True` this method will block until a detection update is
        available, default `True`

        __Returns__

        - detections: Python list of dictionaries: `{"shape": Shape obj, "location": LocationGlobalRelative obj}`
        """
        return self._queue.get(block, timeout)

    def get_current_frame(self, timeout, block=True):
        """
        Returns the current frame from the OpenCV video stream.

        __Parameters__

        - timeout: How long this method may block before it throws a
        `Queue.Empty` exception
        - block: When `True` this method will block until a detection update is
        available, default `True`

        __Returns__

        - frame: OpenCV compatible frame (NumPy)
        """
        return self._frame_buffer.get(block, timeout)
