#!/usr/bin/python
# filter.py conflicts with builtin 'filter()' function

"""
The videostream contains a lot of noise and needs to be optimized before
recognizing and calculating the detected objects. This module does just that
using several fitler algorithmes from OpenCV.

- [bilateralFilter](https://docs.opencv.org/3.3.0/d4/d86/group__imgproc__filter.html#ga9d7064d478c95d60003cf839430737ed)
- [inRange](https://docs.opencv.org/3.3.0/d2/de8/group__core__array.html#ga48af0ab51e36436c5d04340e036ce981)
- [morphologyEx](https://docs.opencv.org/3.3.0/d4/d86/group__imgproc__filter.html#ga67493776e3ad1a3df63883829375201f)

More information about filtering images in OpenCV can be found [here](https://docs.opencv.org/3.1.0/d4/d13/tutorial_py_filtering.html).
"""

from aeroteam.constants import *
from aeroteam.models import *
import numpy as np
import cv2

# Expose only public classes
__all__ = ["Filter", "InvalidContoursException"]

class InvalidContoursException(Exception):
    pass

class Filter(object):
    """
    Creates a Filter instance with the default values defined in the Constants
    module.

    __Parameters (optional)__

    - bil_pxl_dia: OpenCV Bilateral filter pixel diameter.
    - bil_sig_color: OpenCV Bilateral filter sigma color.
    - bil_sig_space: OpenCV Bilateral filter sigma space.

    """
    def __init__(self, bil_pxl_dia=FILTER_BIL_PXL_DIA,
        bil_sig_color=FILTER_BIL_SIG_COLOR, \
        bil_sig_space=FILTER_BIL_SIG_SPACE, morph_area=FILTER_MORPH_AREA):
        self.bilateral_pixel_diameter = bil_pxl_dia
        self.bilateral_sigma_color = bil_sig_color
        self.bilateral_sigma_space = bil_sig_space
        self.morphologyEx_area = morph_area

    def __str__(self):
        return "Filter(PXL_DIA={0}, SIG_COLOR={1}, SIG_SPACE={2}, MORPH_AREA={3})"\
        .format(self.bilateral_pixel_diameter, self.bilateral_sigma_color, \
        self.bilateral_sigma_space, self.morphologyEx_area)

    def bilateral(self, frame):
        """
        Applies an OpenCV Bilateral filter on the frame and returns the result.

        __Parameters__

        - frame: an OpenCV compatible videoframe in BGR mode

        __Returns__

        - frame: an OpenCV compatible videoframe in BGR mode
        """
        return cv2.bilateralFilter(frame, self.bilateral_pixel_diameter, self.bilateral_sigma_color, self.bilateral_sigma_space)

    def remove_grass(self, frame):
        """
        Removes the grass from an OpenCV frame, the drone will see a lot of grass
        in the background while detecting shapes. To avoid any influence of the
        grass on the shape detection we use this method to remove it. This is
        accomplished by using a mask for each color we want to track. By
        comparing the mean values of each mask we can detect which color is
        detected in the frame.

        __Parameters__

        - frame: an OpenCV compatible videoframe in BGR mode

        __Returns__

        - frame: an OpenCV compatible videoframe in BGR mode
        - color: Color enum representing the color of the used mask
        """
        # Create a mask for the green grass color
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Filter out any red colors
        min_red_low = np.array([0, 50, 100], dtype=np.uint8)
        max_red_low = np.array([20, 255, 255], dtype=np.uint8)
        min_red_high = np.array([140, 50, 100], dtype=np.uint8)
        max_red_high = np.array([180, 255, 255], dtype=np.uint8)

        # Filter out any yellow colors
        min_yellow = np.array([25, 100, 200], dtype=np.uint8)
        max_yellow = np.array([40, 255, 255], dtype=np.uint8)

        mask_red_low = cv2.inRange(hsv, min_red_low, max_red_low)
        mask_red_high = cv2.inRange(hsv, min_red_high, max_red_high)
        grass_mask_red = mask_red_low | mask_red_high # Red is twice in HSV
        grass_mask_yellow = cv2.inRange(hsv, min_yellow, max_yellow)

        # Remove small dots in the mask
        area = (self.morphologyEx_area, self.morphologyEx_area)
        grass_mask_red = cv2.morphologyEx(grass_mask_red, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, area))
        grass_mask_yellow = cv2.morphologyEx(grass_mask_yellow, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, area))

        # Select the right mask
        mean_red = grass_mask_red.mean()
        mean_yellow = grass_mask_yellow.mean()

        height, width = frame.shape[:2]
        grass_mask = np.zeros((height,width,1), np.uint8) # blank gray image
        color = Color.UNKNOWN
        # Both masks detected nothing
        if(mean_red < FILTER_MASK_EMPTY_THRESH and mean_yellow < FILTER_MASK_EMPTY_THRESH):
            pass
        # Red mask hit
        elif(mean_red > mean_yellow):
            grass_mask = grass_mask_red
            color = Color.RED
        # Yellow mask hit
        else:
            grass_mask = grass_mask_yellow
            color = Color.YELLOW

        # Apply the mask on the frame
        return cv2.bitwise_and(frame, frame, mask=grass_mask), color

    def remove_false_contours(self, contours, frame):
        """
        Shapes always take a large amount of space in each frame.
        When an empty frame might be passed to OpenCV we need to filter it out
        by checking if the detected contours are a large area of the frame.

        __Parameters__

        - contours: a list of contours which will be drawn on the frame from
        find_contours()
        - frame: an OpenCV compatible videoframe in BGR mode

        __Returns__

        - main_contour: the shape contour after filtering. Is None when
        main_contour is not covering enough of the frame

        __Raises__

        - UnknownColorException: when the contours list is empty.
        This happens when a blue rectangle is masked for yellow and red colors,
        a black frame is returned by self.remove_grass().
        """
        # Find the size of the frame
        xSize, ySize = frame.shape[:2]
        frame_area = xSize * ySize

        # Determine the main contour, we assume this is the object we want to
        if(len(contours) == 0):
            raise UnknownColorException("Contours list is empty, color is probably invalid!")

        main_contour = contours[0] # main contour

        # Find the contourArea, the biggest is our main contour
        # https://docs.opencv.org/3.3.0/d3/dc0/group__imgproc__shape.html#ga2c759ed9f497d4a618048a2f56dc97f1
        max_area = cv2.contourArea(main_contour)

        # Scan the other contours to check if there's a bigger one
        for cont in contours:
            if cv2.contourArea(cont) > max_area:
                main_contour = cont
                max_area = cv2.contourArea(cont)

        # If the frame isn't empty then we should have the shape contour now in
        # main_contour variable. If it's empty, the contour area will be
        # incredible small compared to the frame area.
        #print("Contour area: {0}".format(cv2.contourArea(main_contour)))
        #print("Frame area cropped: {0}".format(frame_area*FILTER_CONTOUR_AREA))
        if(cv2.contourArea(main_contour) > frame_area*FILTER_CONTOUR_AREA):
            return main_contour

        # No valid main contours found
        #print("No valid main countours found")
        return []
