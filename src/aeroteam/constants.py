#!/usr/bin/python
"""
All constants used in the aeroteam module.
"""
import os

# global
BLUE = (255, 0, 0)
GREEN = (0, 255, 0)
RED = (0, 0, 255)
YELLOW = (0, 255, 255)
ORANGE = (0, 165, 255)
PURPLE = (255, 0 , 255)
BOUNDRY_BOX_UPSCALE = 0.33
DEFAULT_ALTITUDE = 4.0 # 4.0 m
DEFAULT_AIRSPEED = 1.0 # 1.0 m/s
DEFAULT_GROUNDSPEED = 1.0 # 1.0 m/s
DEFAULT_DROP_ALTITUDE = 1.5 # 1.5 m

# processor.py
PROCESSOR_MIN_CENTER = 0.5 # 50% max of the center
PROCESSOR_MAX_CENTER = 1.5 # 150% max of the center
PROCESSOR_MIN_FRAMES_HIT = 3 # at least 3 frames are needed to confirm a shape

# detector.py
DETECT_TRESHOLD = { "min":127, "max":255 }
DETECT_BLUR = 5
DETECT_LIN_EPS = 0.025 # accuracy approxPolyDP 5% of arclength (see https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html)
DETECT_RED_H = [[0,25], [150,180]] # Red in HSV (see https://docs.opencv.org/3.2.0/df/d9d/tutorial_py_colorspaces.html)
DETECT_YELLOW_H = [[25,70]] # Yellow in HSV
DETECT_MIN_S = 20
DETECT_MIN_V = 20
DETECT_WHITE_THRESHOLD = 127
DETECT_HUE_RANGE = 10
RECTANGLE_POINTS = [4]
ARROW_POINTS = [6,7,8,9] # ----> small line of the arrow accounted for 1 point
CROSS_POINTS = [10,11,12] # ----> small line of the each side can be accounted for 1 or 2 points
POINT_SIZE = 5
LINE_SIZE = 1

# filtering.py
FILTER_MASK_EMPTY_THRESH = 1.0
FILTER_BIL_PXL_DIA = 5 # Large filters are very slow
FILTER_BIL_SIG_COLOR = 75
FILTER_BIL_SIG_SPACE = 75
FILTER_MORPH_AREA = 7
FILTER_CONTOUR_AREA = 0.005 # Shape must be at least covering 0,1% of the whole frame area

# shapes.py
SHAPE_GRAY_AREA = 0.025 # [-1.25%, +1.25%] of the image width (centered)
SHAPE_ARROW_SIZE_RATIO = {"min": 1.1, "max": 2.5 } # 42cm x 100cm MIN
SHAPE_RECT_SIZE_RATIO = {"min": 0.2, "max": 3.5} # 20cm x 100cm MIN
SHAPE_CROSS_SIZE_RATIO = {"min": 0.9, "max":1.1 } # 100cm x 100cm

# drone.py
DRONE_MAX_DELTA_DISTANCE = 0.75 # maximum difference in metres between the target and the real location
DRONE_MAX_PERCENTAGE_DISTANCE = 0.01 # maximum 1% difference when we need to travel far
DRONE_MAX_PERCENTAGE_ALTITUDE = 0.975 # Reach at least 97.5% of the target altitude when taking off
DRONE_MAX_DELTA_ALTITUDE = 0.1 # maximum tolerance on the inaccurate altitude (metres)
DRONE_MAX_LANDING_ALTITUDE = 0.1 # maximum tolerance when landing (metres)
DRONE_DROP_OPEN = 1000 # servo angle in ms to open bomber (1000 - 2000 ms)
DRONE_DROP_CLOSE = 2000 # servo angle in ms to open bomber (1000 - 2000 ms)
DRONE_DROP_SERVO = 1 # servo ID on the Pixhawk connected to our bomber system
DRONE_DROP_WAIT = 5 # how long the drone needs to wait when performing a dropping (seconds)

# model.py
MODEL_TERMINAL_MAX_LINES = 10 # Maxiumum text lines in de GUI
MODEL_TERMINAL_UPDATE_INTERVAL = 0.20 # Update interval for the terminal output
MODEL_ICON_PATH = "./images/{0}-{1}.png"

# tools.py
AOVDRONE = 60           #Angle Of View of the camera mounted to the drone, in degrees (LONG SIDE)
EARTHRADIUS = 6371000   #Radius of the earth, in meters
MINIMUMALTITUDE = 2.0   #Minimum altitude for camera calculations
FIELDSIDE = 15.0        #The field that has to be scanned is 15m x 15m
CENTREBAND = 1.2        #Camera only looks at the inner 1.2m x 1.2m of the image
