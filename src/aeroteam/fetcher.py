#!/usr/bin/python
"""
Fetching frames from the onboard camera is a costly operation for the CPU.
I/O is slow and might be the bottleneck for the frame detection to achieve a
high number of FPS. By running the fetcher in a seperate process we can contain
this issue without blocking the OpenCV detector.
"""

import threading
import signal
import cv2
from Queue import Queue

__all__ = ["Fetcher"]

class FetcherThread(threading.Thread):
    """
    Provides a Fetcher thread for OpenCV.

    __Parameters__

    - queue: Python `Queue.Queue()` object to save the captured frames
    - cam: path of the camera, for example: `/dev/video0` or `0`
    """
    def __init__(self, queue, cam):
        super(FetcherThread, self).__init__()
        self.daemon = True # Python won't exit until this thread is finished
        self.shutdown = threading.Event()
        self.queue = queue
        self.cam = cam

    def run(self):
        """
        Captures the camera frames and pushes them to the queue.
        """
        # Acquire the video stream
        import os
        print(os.path.abspath(self.cam))
        cam = cv2.VideoCapture(self.cam)

        # Read and push all the frames to the queue
        while(not(self.shutdown.is_set()) and cam.isOpened()):
            # When reading fails due a disconnection, end of file, ...
            # we shutdown the Fetcher thread
            ret, frame = cam.read()
            if not(ret):
                print("End of Stream")
                break

            # Make some space in the buffer if needed
            # Push the frame to the buffer
            if self.queue.full():
                self.queue.get()
            self.queue.put(frame)

        # Release our resource when shutting down
        cam.release()

class Fetcher(object):
    """
    Allows the user the fetch the frames from the camera without interrupting
    the main process. Without this, the main method would be much slower since
    I/O operations can take a lot of time.

    __Parameters__

    - cam: path of the camera, for example: `/dev/video0` or `0`
    """
    def __init__(self, cam):
        self.cam = cam

        # Register signal handlers
        try:
            signal.signal(signal.SIGTERM, self.stop)
            signal.signal(signal.SIGINT, self.stop)
        except ValueError:
            print("Signals not registered since we aren't running in the main thread")

        # Start reading the camera frames
        self._queue = Queue(60) # max 30 frames buffer
        self._worker = FetcherThread(self._queue, self.cam)
        self._worker.start()

    def get_frame(self, block=True, timeout=0):
        """
        Fetches a frame from the queue and returns it.

        __Parameters__

        - block: this method blocks until a frame is available in the queue if
        set to `True` (default)

        - timeout: if `block` is set to `True` then you can specify how long
        this method may block before raising a `Queue.Empty` exception
        """
        return self._queue.get(block, timeout)

    def stop(self, signum=None, frame=None):
        """
        Shutdowns the worker thread.
        """
        self._worker.shutdown.set()

        # Avoid blocking Queue when joining (deadlock)
        # https://stackoverflow.com/questions/3121622/python-multiprocessing-queue-deadlocks-on-put-and-get#3571687
        while(not(self._queue.empty())):
            self._queue.get()

        # Wait until our worker is joined
        if(self._worker.is_alive()):
            self._worker.join()
        print("Worker thread ended")
