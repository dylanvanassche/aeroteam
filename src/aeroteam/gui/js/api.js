var HTTPClient = function() {
    this.get = function(url, callback) {
        var request = new XMLHttpRequest();
        request.open("GET", url, true); // asynchronous
        request.onreadystatechange = function() {
          if (request.readyState == 4 && request.status == 200)
            callback(request.responseText);
        };
        request.send();
    };
};

var heartbeat = false;
var previousBombState = true;
var client = new HTTPClient();
var bombAudio = new Audio('../sounds/bomb_dropped.ogg');

function process(response) {
    // Reset timeout
    clearTimeout(connection_lost_timer)

    // Parse JSON string
    json = JSON.parse(response)

    // Update our dynamic HTML
    document.getElementById("battery-percentage").innerHTML = Math.round(json["battery"]) + " %";
    document.getElementById("battery-bar").style.width = Math.round(json["battery"]) + "%";

    var location = "<b>GPS (lat, lon):</b> " + json["position"]["lat"] + "&#176;, " + json["position"]["lon"] + "&#176; <br><b>Altitude:</b> " + json["position"]["alt"] + " metres";
    document.getElementById("location").innerHTML = location

    var terminal = ""; // Terminal output is an array
    for(var i=0; i < json["terminal"].length; i++) {
        terminal +=  json["terminal"][i];
        if(i < json["terminal"].length-1) {
            terminal += "<br>";
        }
    }
    document.getElementById("terminal").innerHTML = terminal;

    // Update map with current position and markers
    var zoom = 17;
    set_position_map(json["position"]["lat"], json["position"]["lon"], zoom);

    // Show our detected shapes in a table
    var table_content = "<tr><th>Shape</th><th>GPS location (lat, lon)</th></tr>"

    for(var i=0; i < json["detections"].length; i++) {
        table_content += _table_shape(json["detections"][i]["icon"], json["detections"][i]["position"])
    }

    document.getElementById("detections").innerHTML = table_content

    // Update camera feed if frame is different
    if(document.getElementById("feed").src.indexOf(json["feed"]) === -1) {
        document.getElementById("feed").src = json["feed"];
    }

    // Play sound and update the GUI when the bomb status has been changed
    if(json["has_bomb"] && previousBombState == false) {
        document.getElementById("has-bomb").innerHTML = "Bomb READY";
    }
    else if(!json["has_bomb"] && previousBombState == true) {
        document.getElementById("has-bomb").innerHTML = "Bomb DROPPED";
        bombAudio.play();
    }
    previousBombState = json["has_bomb"];

    // Disable buttons when executing mission and set the right cancel button
    if(json["busy"]) {
        var m = document.getElementsByClassName("mission");
        for(var i=0; i < m.length; i++) {
            m[i].disabled = true;
        }
        document.getElementById("cancel-button").value = "ABORT";
        document.getElementById("cancel-button").disabled = false;
    }
    else {
        var m = document.getElementsByClassName("mission");
        for(var i=0; i < m.length; i++) {
            m[i].disabled = false;
        }
        document.getElementById("cancel-button").value = "SHUTDOWN";
        document.getElementById("cancel-button").disabled = false;
    }

    // Blink heartbeat
    if(heartbeat) {
        document.getElementById("last-update").classList.remove("w3-green")
        document.getElementById("last-update").classList.add("w3-black")
    }
    else {
        document.getElementById("last-update").classList.remove("w3-black")
        document.getElementById("last-update").classList.add("w3-green")
    }
    heartbeat = !heartbeat;

    // Enable timeout timer again
    connection_lost_timer = setTimeout(connection_lost, 2500);
};

// Poll the webserver for the actual information
function update() {
    client.get("update", process);
};

// Shutdown the webserver
function cancel() {
    var button = document.getElementById("cancel-button").value;
    if(button == "SHUTDOWN") {
        if(confirm("Initiate system shutdown?")) {
            client.get("stop", function() {})
        }
    }
    else if(button == "ABORT") {
        if(confirm("Abort current mission?")) {
            client.get("abort", function() {})
        }
    }
    else {
        alert("Unsupported action!")
    }
}

function _table_shape(icon, location) {
    var table = "<tr>";
    table += "<td><img src='" + icon + "' alt='Detected shape' class='w3-image' id='table-img'></td>";
    table += "<td>" + location["lat"] + "&#176;, " + location["lon"] + " &#176;</td>";
    table += "</tr>"

    return table
}

function _form_select(title, name, options) {
    var form = "<label><h3>" + title + "</h3></label>"
    form += " <select name='" + name + "' required>"
    form += "<option value='' selected disabled hidden>Choose here</option>"
    for(var i=0; i < options.length; i++) {
        form += "<option value='" + options[i] +"'>" + options[i] + "</option>"
    }
    form += "</select>"

    return form
}

function _form_input(title, name) {
    var form = "<label><h3>" + title + "</h3></label>"
    form += "<input name='" + name + "_lat' type='text' required>&#176;, "
    form += "<input name='" + name + "_lon' type='text' required>&#176;"

    return form
}

// Show input dialog before starting a mission
function show_form(response) {
    // Parse JSON string
    json = JSON.parse(response)

    // Start of the form with a title
    var form = " <form action='" + json["endpoint"] + "' class='w3-container center'>"
    form += "<h1>" + json["title"] + "</h1>"

    // Floating image
    console.log(json["plan"])
    form += "<img src='" + json["plan"] + "' alt='Mission plan' id='form-img'>"

    // Target input
    if(json["target"] == true) {
        form += _form_select(json["shape"].title, json["shape"].name, json["shape"].options)
        form += _form_select(json["color"].title, json["color"].name, json["color"].options)
    }

    // Coordinates input
    for(var i=0; i < json["coordinates"].length; i++) {
        form += _form_input(json["coordinates"][i].title, json["coordinates"][i].name)
    }

    // Submit and abort buttons
    form += "<br><br>"
    form += "<input type='submit' value='START' class='w3-button w3-green'> "
    form += "<input type='button' onclick='hide_form()' value='ABORT' class='w3-button w3-red'>"

    // End of the form and apply it
    form += "</form>"
    document.getElementById("mission-input").innerHTML = form

    // Show overlay
    document.getElementById("mission-input").style.width = "100%";
    document.getElementById("mission-input").style.display = "block";
}

function hide_form() {
    document.getElementById("mission-input").style.display = "none";
}

// Disable all buttons and show alert when connection is lost
function connection_lost() {
    var m = document.getElementsByClassName("mission");
    for(var i=0; i < m.length; i++) {
        m[i].disabled = true;
    }
    document.getElementById("cancel-button").disabled = true;
    alert("Connection timeout!");
}

// Repeat update function every 50ms to achieve 20 FPS
setInterval(update, 50);
// Timeout function
var timeout = false;
var connection_lost_timer = setTimeout(connection_lost, 2500);
