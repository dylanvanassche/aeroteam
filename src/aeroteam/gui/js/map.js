var from_projection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
var to_projection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

function init_map(latitude, longitude, zoom) {
    map = new OpenLayers.Map("map");
    markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(new OpenLayers.Layer.OSM());
    map.addLayer(markers);
    map.zoomToMaxExtent();
    set_position_map(latitude, longitude, zoom);
}

function set_position_map(latitude, longitude, zoom) {
    var position = new OpenLayers.LonLat(longitude, latitude).transform(from_projection, to_projection);
    markers.clearMarkers(); // Remove the older markers before moving the map
    markers.addMarker(new OpenLayers.Marker(position));
    map.setCenter(position, zoom);
}
