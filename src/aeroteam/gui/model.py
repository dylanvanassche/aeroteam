#!/usr/bin/python

import sys
import time
import threading
import signal
import base64
import cv2
from cStringIO import StringIO # C implementation (https://docs.python.org/2/library/stringio.html)
from Queue import Queue, Empty
from aeroteam.constants import *
from aeroteam.models import Color, Rectangle, Arrow, Cross, Mission1, Mission2, Mission3
from dronekit import LocationGlobalRelative

"""
The Model contains the backend of the MVC system.
"""

__all__ = ["Model", "BusyException"]

class BusyException(Exception):
    """
    Starting a new mission when the drone is already busy with another mission
    will raise a BusyException.
    """
    pass

class SniffingThread(threading.Thread):
    """
    Provides a sniffing thread for stdout.

    __Parameters__

    - queue: Python `Queue.Queue()` object for storing the stdout data
    """
    def __init__(self, queue):
        super(SniffingThread, self).__init__()
        self.daemon = True # Python won't exit until this thread is finished
        self.shutdown = threading.Event()
        self.queue = queue
        self.redirect = StringIO()

    def run(self):
        """
        Redirects the stdout output to a StringIO file.
        The thread copies the lastest output and prints it back to stdout.
        """
        # Redirect the stdout to our StringIO file
        sys.stdout = self.redirect
        old_line = 0

        # Sniffing until shutdown flag is set from the main thread
        while(not(self.shutdown.is_set())):
            # Thread safety for clearing a queue
            # https://stackoverflow.com/questions/6517953/clear-all-items-from-the-queue#6518011
            self.queue.mutex.acquire()
            self.queue.queue.clear()
            self.queue.all_tasks_done.notify_all()
            self.queue.unfinished_tasks = 0
            self.queue.mutex.release()

            # Put the last MODEL_TERMINAL_MAX_LINES lines on the queue
            output = self.redirect.getvalue().splitlines()
            self.queue.put(output[-MODEL_TERMINAL_MAX_LINES:])

            # Pipe the output to stdout and flush
            current_line = len(output)
            for line in range(old_line, current_line):
                sys.__stdout__.write(output[line] + "\n")
            sys.__stdout__.flush()
            old_line = current_line

            # Wait for next update
            time.sleep(MODEL_TERMINAL_UPDATE_INTERVAL)

        # Remove redirection on exit
        sys.stdout = sys.__stdout__

class Model(object):
    """
    The Model object captures the terminal output using a seperate worker thread,
    updates the drone data using observers, ...

    __Parameters__

    - controller: Controller instance
    - drone: Drone instance
    - camera_connection: camera connection, for example: `/dev/video0`
    """
    def __init__(self, controller, drone, camera_connection):
        self.current_mission = None
        self._controller = controller
        self._battery = 0.0
        self._position = LocationGlobalRelative(0.0, 0.0, 0.0)
        self._busy = False
        self._terminal = ["Loading ..."]
        self._detections = []

        # Register signal handlers
        signal.signal(signal.SIGTERM, self.stop)
        signal.signal(signal.SIGINT, self.stop)

        # Start sniffing of stdout
        self._queue = Queue()
        self._worker = SniffingThread(self._queue)
        self._worker.start()

        # Save a reference to our Drone and Processor instances
        self._drone = drone
        self._camera = camera_connection

        # Connect the drone observers
        self._drone.add_observer("battery", self._battery_observer)
        self._drone.add_observer("location.global_relative_frame", self._position_observer)

    def _battery_observer(self, dronekit_self, name, msg):
        """
        Callback method for Python DroneKit to observer the battery level.
        """
        lvl = msg.level
        if lvl is not None:
            self._battery = lvl
        else:
            self._battery = 0.0
            print("Battery unknown (DroneKit)")

    def _position_observer(self, dronekit_self, name, msg):
        """
        Callback method for Python DroneKit to observer the GPS position.

        __Parameters__

        - dronekit_self: `self` parameter of the DroneKit module
        - name: observer name
        - msg: observer data
        """
        pos = msg
        if pos is not None:
            self._position = pos
        else:
            self._position = LocationGlobalRelative(0.0, 0.0, 0.0)
            print("Location unknown (DroneKit)")

    def add_detections(self, data):
        """
        Converts the detections data for the Web GUI.
        Every element from the detections data is expected to be a Python
        dictionary: `{"shape": Shape, "location": LocationGlobalRelative}`.

        __Parameters__

        - data: a Python list of dictionaries
        """
        # Clear the previous detections
        if len(data) > 0:
            self._detections = []

            for d in data:
                shape = d["shape"]
                location = d["location"]
                detection = {
                    "icon": "",
                    "position": {"lat": 0.0, "lon": 0.0}
                }
                s = None
                c = None

                # Check the shape type
                if isinstance(shape, Rectangle):
                    s = "rectangle"
                elif isinstance(shape, Cross):
                    s = "cross"
                elif isinstance(shape, Arrow):
                    s = "arrow"
                else:
                    s = "unknown"

                # Check the color type
                if shape.color == Color.RED:
                    c = "red"
                elif shape.color == Color.YELLOW:
                    c = "yellow"
                else:
                    c = "unknown"

                detection["icon"] = MODEL_ICON_PATH.format(s, c)
                detection["position"]["lat"] = location.lat
                detection["position"]["lon"] = location.lon

                self._detections.append(detection)

    def stop(self, signum=None, frame=None):
        """
        Shutdowns the worker thread and disconnects the observers.
        """
        self._worker.shutdown.set()

        # Avoid blocking Queue when joining (deadlock)
        # https://stackoverflow.com/questions/3121622/python-multiprocessing-queue-deadlocks-on-put-and-get#3571687
        while(not(self._queue.empty())):
            self._queue.get()

        # Wait until our worker is joined
        if(self._worker.is_alive()):
            self._worker.join()
        print("Worker thread ended")

        # Disconnect our observers
        self._drone.remove_observer("battery", self._battery_observer)
        self._drone.remove_observer("location", self._position_observer)
        print("Observers disconnected")

        # Close the connection with the drone
        self._drone.stop()

    def mission1(self, startpoint, endpoint, color, shape):
        """
        Starts mission 1: Choose the right target.
        You can retrieve the current mission using `self.current_mission`.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - endpoint: GPS location of the landing location (LocationGlobalRelative)
        - color: color of the shape as the bomb target (Color enum)
        - shape: Type of shape as the bomb target (Shape)

        __Raises__

        - BusyException: only 1 mission at the same time can be executed by the
        drone
        """
        self.set_busy(True)
        self.current_mission = Mission1(self._drone, self._camera)
        self.current_mission.run(startpoint, endpoint, color, shape)

    def mission2(self, startpoint, p1):
        """
        Starts mission 2: Triangulate my location.
        You can retrieve the current mission using `self.current_mission`.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - p1: GPS location of the fake waypoint (LocationGlobalRelative)

        __Raises__

        - BusyException: only 1 mission at the same time can be executed by the
        drone
        """
        self.set_busy(True)
        self.current_mission = Mission2(self._drone, self._camera)
        self.current_mission.run(startpoint, p1)

    def mission3(self, startpoint, p1, p2, p3, p4, color, shape):
        """
        Starts mission 3: Search and destroy.
        You can retrieve the current mission using `self.current_mission`.

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - p1: left-bottom GPS location of the search area (LocationGlobalRelative)
        - p2: left-top GPS location of the search area (LocationGlobalRelative)
        - p3: right-top GPS location of the search area (LocationGlobalRelative)
        - p4: right-bottom GPS location of the search area (LocationGlobalRelative)
        - color: color of the shape as the bomb target (Color enum)
        - shape: Type of shape as the bomb target (Shape)

        __Raises__

        - BusyException: only 1 mission at the same time can be executed by the
        drones
        """
        self.set_busy(True)
        self.current_mission = Mission3(self._drone, self._camera)
        self.current_mission.run(startpoint, p1, p2, p3, p4)

    def tests(self, startpoint, endpoint, p1, p2, p3, p4, shape, color):
        """
        Starts tests mission.
        You can retrieve the current mission using `self.current_mission`.

        _Warning_: Not implemented

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - endpoint: GPS location of the landing location (LocationGlobalRelative)
        - p1: left-bottom GPS location of the search area (LocationGlobalRelative)
        - p2: left-top GPS location of the search area (LocationGlobalRelative)
        - p3: right-top GPS location of the search area (LocationGlobalRelative)
        - p4: right-bottom GPS location of the search area (LocationGlobalRelative)
        - shape: Type of shape as the bomb target (Shape)
        - color: color of the shape as the bomb target (Color enum)

        __Raises__

        - BusyException: only 1 mission at the same time can be executed by the
        drone
        """
        self.set_busy(True)
        #self.current_mission = MissionUnittest(self._drone, self._processor)
        #self.current_mission.run(startpoint, endpoint, p1, p2, p3, p4, shape, color)

    def abort(self):
        """
        Aborts the current mission, the drone will land immediately!
        `self.current_mission` will be `None` after calling this method.
        """
        self.current_mission = None
        self._drone.land()
        self.set_busy(False)
        print("Aborting current mission OK")

    def get_terminal(self):
        """
        Retrieves the lastest stdout data and returns it.

        __Returns__

        - terminal: Python list containing the latests terminal output from
        stdout
        """
        # If output is available, update it
        if not self._queue.empty():
            self._terminal = self._queue.get()

        # Return the last cached output
        return self._terminal

    def get_battery(self):
        """
        Retrieves the latest battery data and returns it.

        __Returns__

        - terminal: float representing the battery percentage [0.0, 100.0]
        """
        return self._battery

    def get_position(self):
        """
        Retrieves the latest GPS position data and returns it.

        __Returns__

        - position: Python tuple (latitude, longitude, altitude)
        """
        return self._position

    def get_feed(self):
        """
        Retrieves the latest camera feed frame in `base64` encoding and returns it.
        You can find more information [here](https://stackoverflow.com/questions/18943678/showing-web-camera-through-web-interface-python-cherrypy-opencv)

        __Returns__

        - feed: camera frame in `base64` encoding
        """
        # Encode image in OpenCV
        if self.current_mission is not None and self.current_mission.processor is not None:
            try:
                _, data = cv2.imencode('.jpg', self.current_mission.processor.get_current_frame(timeout=0.33))

                # Encode to base64 and return it
                jpeg_base64 = base64.b64encode(data.tostring())
                return 'data:image/jpeg;base64,%s' % (jpeg_base64)
            except Empty as e:
                pass

        return "images/loading.gif"

    def get_busy(self):
        """
        Retrieves the latest busy state and returns it.

        __Returns__

        - busy: boolean representing if the drone is executing a mission or not
        """
        return self._busy

    def get_detections(self):
        """
        Retrieves the detected shapes from the current mission and returns it.

        __Returns__

        - detections: Python list of the current detected shapes
        """
        if self.current_mission is not None and self.current_mission.processor is not None:
            detected = []
            try:
                detected = self.current_mission.processor.get_detections(timeout=0.25)
                self.add_detections(detected)
            except Empty as e:
                pass
            return self._detections
        else:
            return []

    def has_bomb(self):
        """
        Retrieves the current bomb state if a mission is running.
        When no mission is running, this method returns default `True`.

        __Returns__

        - state: `True` when ready to drop the bomb, `False` when already dropped
        """
        if self.current_mission is None:
            return True
        else:
            return self.current_mission.has_bomb()

    def set_busy(self, state):
        """
        Sets latest busy state.

        __Raises__

        - BusyException: only 1 mission at the same time can be executed by the
        drone
        """
        if self._busy and state:
            raise BusyException("Only one mission can be executed at the same time")
        self._busy = state
