#!/usr/bin/python

"""
The View contains everything the user will see.
"""

import cherrypy
import os
import json
import time
from dronekit import LocationGlobalRelative
from aeroteam.gui.model import BusyException
from aeroteam.models import Shape, Rectangle, Arrow, Cross, Color

__all__ = ["View"]

class View(object):
    """
    The View object exposes a HTML index page and a JSON interface for dynamic
    updates.
    """
    def __init__(self, controller):
        self._controller = controller
        self.busy = False
        self.longitude = 0.0
        self.latitude = 0.0
        self.altitude = 0.0
        self.battery = 0.0
        self.terminal = []

    def _parse_target(self, color, shape):
        """
        Parses the target data from strings.

        __Parameters__

        - color: color of the target (string)
        - shape: target shape (string)

        __Returns__

        - color: color of the target (Color enum)
        - shape: target shape (subclass of Shape)
        """
        # Default values
        c = Color.UNKNOWN
        s = Shape

        # Parse the color
        if color == "red":
            c = Color.RED
        elif color == "yellow":
            c = Color.YELLOW

        # Parse the shape
        if shape == "rectangle":
            s = Rectangle
        elif shape == "arrow":
            s = Arrow
        elif shape == "cross":
            s = Cross

        print("Converted to objects:")
        print(c,s)
        return c, s

    @cherrypy.expose
    def index(self):
        """
        Retrieves the `index.html` page.

        __Returns__

        - index: the `index.html` file
        """
        # Relative path from where the Controller is started
        return file(os.path.join(self._controller.root, "html/index.html"))

    @cherrypy.expose
    def update(self):
        """
        Retrieves the latest data and returns it as JSON.

        __Returns__

        - data: JSON encoded data
        """
        location = self._controller.get_position()
        self.longitude = location.lon
        self.latitude = location.lat
        self.altitude = location.alt
        self.battery = self._controller.get_battery()
        self.terminal = self._controller.get_terminal()
        self.feed = self._controller.get_feed()
        self.busy = self._controller.get_busy()
        self.detections = self._controller.get_detections()
        self.has_bomb = self._controller.has_bomb()
        output = {
            "terminal": self.terminal,
            "battery": self.battery,
            "position": {"lat": self.latitude, "lon": self.longitude, "alt": self.altitude },
            "feed": self.feed,
            "busy": self.busy,
            "detections" : self.detections,
            "has_bomb": self.has_bomb,
            "timestamp": time.time()
        }
        return json.dumps(output)

    @cherrypy.expose
    def mission_form(self, endpoint):
        """
        Prepares a mission form depending on the type of mission.

        __Returns__

        - data: JSON encoded data
        """
        # tests
        data = {
            "plan": "./images/tests.jpeg",
            "endpoint": endpoint,
            "title": "Tests",
            "target": True,
            "shape": {"title": "Shape", "name": "shape", "options": ["rectangle", "arrow", "cross"]},
            "color": {"title": "Color", "name": "color", "options": ["red", "yellow"]},
            "coordinates": [{
                "title": "Point T (lat, lon)",
                "name": "point_t"
            },
            {
                "title": "Point L (lat, lon)",
                "name": "point_l"
            },
            {
                "title": "Point 1 (lat, lon)",
                "name": "point_1"
            },
            {
                "title": "Point 2 (lat, lon)",
                "name": "point_2"
            },
            {
                "title": "Point 3 (lat, lon)",
                "name": "point_3"
            },
            {
                "title": "Point 4 (lat, lon)",
                "name": "point_4"
            }]
        }

        # mission 1: Choose-the-Good-Target
        if(endpoint == "mission1"):
            data["plan"] = "./images/mission1.png"
            data["title"] = "Mission 1: Choose-the-Good-Target"
            data["target"] = True
            data["coordinates"] = [{
                "title": "Point T (lat, lon)",
                "name": "point_t"
            },
            {
                "title": "Point L (lat, lon)",
                "name": "point_l"
            }]

        # mission 2: Triangulate-my-Landing-Area
        if(endpoint == "mission2"):
            data["plan"] = "./images/mission2.png"
            data["title"] = "Mission 2: Triangulate-my-Landing-Area"
            data["target"] = False
            data["coordinates"] = [{
                "title": "Point T (lat, lon)",
                "name": "point_t"
            },
            {
                "title": "Point 1 (lat, lon)",
                "name": "point_1"
            }]

        # mission 3: Full mission
        elif(endpoint == "mission3"):
            data["plan"] = "./images/mission3.png"
            data["title"] = "Mission 3: Full mission"
            data["target"] = True
            data["coordinates"] = [{
                "title": "Point T (lat, lon)",
                "name": "point_t"
            },
            {
                "title": "Point 1 (lat, lon)",
                "name": "point_1"
            },
            {
                "title": "Point 2 (lat, lon)",
                "name": "point_2"
            },
            {
                "title": "Point 3 (lat, lon)",
                "name": "point_3"
            },
            {
                "title": "Point 4 (lat, lon)",
                "name": "point_4"
            }]

        return json.dumps(data)

    @cherrypy.expose
    def mission1(self, point_t_lat, point_t_lon, point_l_lat, point_l_lon, color, shape):
        """
        Starts mission 1 with the given parameters.

        __Parameters__

        - point_t_lat: point T latitude
        - point_t_lon: point T longitude
        - point_l_lat: point L latitude
        - point_l_lon: point L longitude
        - color: bomb target color
        - shape: bomb target shape
        """
        try:
            print("Starting mission 1...")

            # Parsing
            pos = self._controller.get_position()
            print(point_t_lat)
            print(point_t_lon)
            print(point_l_lat)
            print(point_l_lon)
            print(pos)
            print(pos.alt)
            t = LocationGlobalRelative(float(point_t_lat), float(point_t_lon), pos.alt)
            l = LocationGlobalRelative(float(point_l_lat), float(point_l_lon), pos.alt)
            c, s = self._parse_target(color, shape)

            # Executing
            self._controller.mission1(t, l, c, s)
        except BusyException as e:
            print(e)
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def mission2(self, point_t_lat, point_t_lon, point_1_lat, point_1_lon):
        """
        Starts mission 2 with the given parameters.

        __Parameters__

        - point_t_lat: point T latitude
        - point_t_lon: point T longitude
        - point_1_lat: point 1 latitude
        - point_1_lon: point 1 longitude
        """
        try:
            print("Starting mission 2...")

            # Parsing
            latitude, longitude, altitude = self._controller.get_position()
            t = LocationGlobalRelative(float(point_t_lat), float(point_t_lon), altitude)
            p1 = LocationGlobalRelative(float(point_1_lat), float(point_1_lon), altitude)

            # Executing
            self._controller.mission2(t, p1)
        except BusyException as e:
            print(e)
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def mission3(self, point_t_lat, point_t_lon, point_1_lat, point_1_lon, point_2_lat, point_2_lon, point_3_lat, point_3_lon, point_4_lat, point_4_lon, color, shape):
        """
        Starts mission 3 with the given parameters.

        __Parameters__

        - point_t_lat: point T latitude
        - point_t_lon: point T longitude
        - point_1_lat: point 1 latitude
        - point_1_lon: point 1 longitude
        - point_2_lat: point 2 latitude
        - point_2_lon: point 2 longitude
        - point_3_lat: point 3 latitude
        - point_3_lon: point 3 longitude
        - point_4_lat: point 4 latitude
        - point_4_lon: point 4 longitude
        - color: bomb target color
        - shape: bomb target shape
        """
        try:
            print("Starting mission 3...")

            # Parsing
            latitude, longitude, altitude = self._controller.get_position()
            t = LocationGlobalRelative(float(point_t_lat), float(point_t_lon), altitude)
            p1 = LocationGlobalRelative(float(point_1_lat), float(point_1_lon), altitude)
            p2 = LocationGlobalRelative(float(point_2_lat), float(point_2_lon), altitude)
            p3 = LocationGlobalRelative(float(point_3_lat), float(point_3_lon), altitude)
            p4 = LocationGlobalRelative(float(point_4_lat), float(point_4_lon), altitude)
            c, s = self._parse_target(color, shape)

            # Executing
            self._controller.mission3(t, p1, p2, p3, p4, c, s)
        except BusyException as e:
            print(e)
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def tests(self, point_t_lat, point_t_lon, point_l_lat, point_l_lon, point_1_lat, point_1_lon, point_2_lat, point_2_lon, point_3_lat, point_3_lon, point_4_lat, point_4_lon, color, shape):
        """
        Starts mission unittest with the given parameters.

        __Parameters__

        - point_t_lat: point T latitude
        - point_t_lon: point T longitude
        - point_l_lat: point L latitude
        - point_l_lon: point L longitude
        - point_1_lat: point 1 latitude
        - point_1_lon: point 1 longitude
        - point_2_lat: point 2 latitude
        - point_2_lon: point 2 longitude
        - point_3_lat: point 3 latitude
        - point_3_lon: point 3 longitude
        - point_4_lat: point 4 latitude
        - point_4_lon: point 4 longitude
        - color: bomb target color
        - shape: bomb target shape
        """
        try:
            print("Starting unittest mission...")

            # Parsing
            latitude, longitude, altitude = self._controller.get_position()
            t = LocationGlobalRelative(float(point_t_lat), float(point_t_lon), altitude)
            l = LocationGlobalRelative(float(point_l_lat), float(point_l_lon), altitude)
            p1 = LocationGlobalRelative(float(point_1_lat), float(point_1_lon), altitude)
            p2 = LocationGlobalRelative(float(point_2_lat), float(point_2_lon), altitude)
            p3 = LocationGlobalRelative(float(point_3_lat), float(point_3_lon), altitude)
            p4 = LocationGlobalRelative(float(point_4_lat), float(point_4_lon), altitude)
            c, s = self._parse_target(color, shape)

            # Executing
            self._controller.tests(t, l, p1, p2, p3, p4, c, s)
        except BusyException as e:
            print(e)
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def abort(self):
        """
        Aborts the current mission.
        """
        print("Cancelling current mission... Landing immediately!")
        self._controller.abort()
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def stop(self):
        """
        Shutdowns the controller.
        """
        self.busy = self._controller.get_busy()
        if not self.busy:
            print("Shutdown started!")
            self._controller.stop()
        else:
            print("Still processing... Try again later!")
        raise cherrypy.HTTPRedirect("/")
