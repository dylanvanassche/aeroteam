#!/usr/bin/python

# Awesome guy: Kennethreitz
# https://github.com/kennethreitz/setup.py

import io
from setuptools import find_packages, setup, Command

# Package meta-data.
NAME = "aeroteam"
DESCRIPTION = "Aeroteam Python package for the Dassault Drone Challenge 2017-2018"
URL = "https://www.gitlab.com/dylanvanassche/aeroteam"
EMAIL = ""
AUTHOR = "Van Assche Dylan, Laurens Van de Walle, Verschaeren Wout"
REQUIRES_PYTHON = '>=2.7.0'
VERSION = "0.0.2"

# Requirements for Aeroteam
with io.open("requirements.txt") as f:
    REQUIRED = f.read().splitlines()

# Python setup
setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(exclude=("tests",)),
    install_requires=REQUIRED,
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy"
    ]
)
