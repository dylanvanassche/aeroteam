# -*- coding: utf-8 -*-
import numpy as np

from shapely.geometry import Point
from shapely.geometry import LineString
from constants import *

__all__ = ["Tools", "Point", "LowAltitudeException"]
                                #Only the Tools and Point classes and the
                                #LowAltitudeException are available to everyone
                                #Certain classes can be called from within Tools,
                                #using the designated static methods

"""
@author: Wout
"""
class GPScoord(Point):
    """
    Representation of a point in GPS COORDINATES
    Takes in a latitude, longitude and an altitude (in this order)

    __Parameters__

    - lat: decimal representation of the latitude in degrees
    - lon: decimal representation of the longitude in degrees
    - alt: decimal representation of the altitude in meters (optional)
    """
    def __init__(self, latD, lonD, altD = 0):
        self.lat = np.float64(latD)
        self.lon = np.float64(lonD)
        self.alt = np.float64(altD)
        super(GPScoord, self).__init__(self.lat, self.lon)
        
    def __str__(self):
        return "Latitude: " + str(self.lat) + ", Longitude: " + str(self.lon) + ", Altitude: " + str(self.alt)


class Waypoint(object):
    """
    A class to save a waypoint (for example the location of an arrow)

    __Parameters__

    - name: a name or short description of the waypoint
    - gps: the GPS COORDINATES (GPScoord) of the waypoint
    """
    def __init__(self, name, gps):
        self.name = name
        self.gps = gps
    
    def __str__(self):
        return "Waypoint " + self.name + " on location " + str(self.gps)


class LineString2P(LineString):
    """
    Creates a Shapely LineString from 2 Shapely Points
    
    __Parameters__
    
    - p1: A Shapely Point
    - p2: A Shapely Point
    """
    def __init__(self, p1, p2):
        x1 = p1.x
        y1 = p1.y
        x2 = p2.x
        y2 = p2.y
        super(LineString2P, self).__init__([(x1, y1), (x2, y2)])
        
    def __str__(self):
        return "Shapely LineString ((" + str(self.coords[0][0]) + ", " + str(self.coords[0][1]) + "), (" + str(self.coords[1][0]) + ", " + str(self.coords[1][1]) + "))"


class Line(object):
    """
    Representation of a line, can be seen as y = ax + b

    __Parameters__

    - ls: A Shapely LineString or a Shapely Point
    - direction: The angle between the x-axis and the line, CCW
    
    - p1: The first Shapely Point of the LineString
    - p2: The second Shapely Point of the LineString
    """
    def __init__(self, ls, direction = None):
        if(direction == None):
            self.ls = ls
            temp1 = ls.coords[0]
            x1 = temp1[0]
            y1 = temp1[1]
            self.p1 = Point(x1, y1)        
            
            temp2 = ls.coords[1]
            x2 = temp2[0]
            y2 = temp2[1]
            self.p2 = Point(x2, y2)
            
        else:
            #ls is actually a Shapely Point
            self.p1 = ls
            
            angle = np.radians(direction)
            #Arbitrary second Point
            x2 = self.p1.x + 1 * np.cos(angle)
            y2 = self.p1.y + 1 * np.sin(angle)
            self.p2 = Point(x2, y2)
            
            self.ls = LineString2P(self.p1, self.p2)
            
        self.a = self.gradient()
        
        self.b = self.p1.y - self.p1.x * self.a
        
    def __str__(self):
        return "y = " + str(self.a) + " x + " + str(self.b)
        
    def gradient(self):
        """
        Calculates the gradient of the line between the two points p1 and p2
    
        __Returns__
    
        - gr: Returns the gradient
        """
        nom = np.float64(self.p2.y - self.p1.y)
        denom = np.float64(self.p2.x - self.p1.x)
        gr = nom/denom
        return gr
        
    def intersect(self, other):
        """
        Calculates the GPS COORDINATE (GPScoord) of the intersection point of
        2 lines. Make sure that both self and other are Lines in the GPS
        COORDINATE system.
    
        __Parameters__
    
        - self: Line
        - other: Line
    
        __Returns__
    
        - GPScoord(s[0], s[1]): Returns a GPS COORDINATE
        """
        #The equations of the lines have to be in the form c1*v1 + c2*v2 = c3
        #where the c's are constants, and the v's are variables
        #In the case of a Line this gives a*x - y = -b
        cm = np.array([[self.a, -1.0], [other.a, -1.0]])    #coefficient matrix
        o = np.array([-self.b, -other.b])                   #ordinate
        s = np.linalg.solve(cm, o)                          #solution
        return GPScoord(s[0], s[1])


class LowAltitudeException(Exception):
    pass

class Tools(object):
    def __init__(self):
        pass
    
    @staticmethod
    def img2GPS(pt, pos, pixW, pixH, heading):
        """
        Takes a Point in IMAGE COORDINATES, and by using the current position
        in GPS COORDINATES it calculates the GPS COORDINATE of the
        aforementioned Point
        
        __Parameters__
        
        - pt: Point in IMAGE COORDINATES
        - pos: Current position in GPS COORDINATES (GPScoord)
        - pixW: Amount of pixels of the WIDTH of the image
        - pixH: Amount of pixels of the HEIGHT of the image
        - heading: CW angle between the drone and the North
        
        __Returns__
        
        - ptGPS: The same Point but converted to GPS COORDINATES (GPScoord)
        
        __Raises__
        
        - LowAltitudeException: Raises an exception when the altitude is
        below the MINIMUMALTITUDE, because the calculations won't be accurate
        enough in that case
        """
        lat1 = np.radians(pos.lat)
        lon1 = np.radians(pos.lon)
        
        if(pos.alt < MINIMUMALTITUDE):
            raise LowAltitudeException("Altitude is too low for calculations!")
        
        AOVrad = np.radians(AOVDRONE)
        hrad = np.float64(np.radians(heading))
        #length of the image in meters
        length = 2*pos.alt*np.tan(AOVrad/2)
        #factor to convert pixels to meters
        factor = length/pixW
        
        #center of the image in IMAGE COORDINATES
        centre = Point(pixW/2, pixH/2)
        dx = pt.x - centre.x
        # * (-1) because the y-axis is inverted when using image coordinates
        dy = (pt.y - centre.y) * (-1)
        
        #CW angle between the line connecting the centre and the point, and the top of the image
        angle = np.arctan2(dx, dy)
        hrad += angle
        
        #Δx and Δy in meters
        dxm = dx * factor
        dym = dy * factor
        d = (dxm**2 + dym**2)**(1/2.0)            #Euclidian distance
        #d = 20.0                               #Test value, use in place of d
        da = d/EARTHRADIUS                      #Angular distance
        #print "lat1:", lat1, " lon1:", lon1, " da", da, " hrad:", hrad
        
        #Formula: https://www.movable-type.co.uk/scripts/latlong.html#destPoint
        lat2 = np.arcsin(np.sin(lat1)*np.cos(da) + np.cos(lat1)*np.sin(da)*np.cos(hrad))
        lon2 = lon1 + np.arctan2(np.sin(hrad)*np.sin(da)*np.cos(lat1), np.cos(da) - np.sin(lat1)*np.sin(lat2))
        
        ptGPS = GPScoord(np.degrees(lat2), np.degrees(lon2), pos.alt)
        #print ptGPS
        return ptGPS
        
    @staticmethod
    def findIntersect(ls1, ls2):
        """
        Takes in two Shapely LineStrings and returns the intersection point in
        GPS COORDINATES
        
        __Parameters__
        
        - ls1: A Shapely LineString
        - ls2: A Shapely LineString
        
        __Returns__
        
        - l1.intersect(l2): Returns the GPS COORDINATE (GPScoord) of the
        intersection point
        """
        l1 = Line(ls1)
        l2 = Line(ls2)
        return l1.intersect(l2)
        
    @staticmethod
    def waypoint(latD, lonD, altD = 0, name = 0):
        """
        Saves a waypoint: a GPS COORDINATE, can be given a name or description
        as fourth argument
                
        __Parameters__
        
        - latD: decimal representation of the latitude in degrees
        - lonD: decimal representation of the longitude in degrees
        - altD: decimal representation of the altitude in meters (optional)
        - name: a name or short description of the waypoint (optional)
        """
        gps = GPScoord(latD, lonD, altD)
        if(name == 0):
            return gps
        else:
            return Waypoint(name, gps)
        
    @staticmethod
    def lineString2P(p1, p2):
        """
        Creates a Shapely LineString from 2 Shapely Points
        (Calls the separate class)
        
        __Parameters__
        
        - p1: A Shapely Point
        - p2: A Shapely Point
        """
        return LineString2P(p1, p2)
        
    @staticmethod
    def scanPattern(p1, p2, p3, p4):
        """
        Creates a list of Points where the drone has to fly to in order to scan
        the whole area between the 4 points.
        
        __Parameters__
        
        - p1: GPS Coordinate
        - p2: GPS Coordinate
        - p3: GPS Coordinate
        - p4: GPS Coordinate
        
        __Returns__
        
        - chkPts: A list of Points
        
        __Raises__
        
        - TooManyPointsException: Raises an exception if the centreband is too
        small, and therefore the amount of points in the list will be too large
        for the drone to check them all (not enough battery life)
        """
        sideDiv = FIELDSIDE / CENTREBAND
        #print sideDiv
        #print np.ceil(sideDiv)
        if (sideDiv > 14):
            raise TooManyPointsException
        deltaXlat = ((p3.lat - p2.lat) + (p4.lat - p1.lat)) / (2 * sideDiv)
        deltaXlon = ((p3.lon - p2.lon) + (p4.lon - p1.lon)) / (2 * sideDiv)
        deltaYlat = ((p2.lat - p1.lat) + (p3.lat - p4.lat)) / (2 * sideDiv)
        deltaYlon = ((p2.lon - p1.lon) + (p3.lon - p4.lon)) / (2 * sideDiv)
        #print deltaYlon
        
        current = p1
        i = 0
        j = 0
        chkPts = []
        chkPts.append(p1)
        while (i <= np.ceil(sideDiv)):
            while (j < np.ceil(sideDiv)):
                if (i%2 == 0):
                    lat = round(current.lat + deltaYlat, 12)
                    lon = round(current.lon + deltaYlon, 12)
                    newPt = GPScoord(lat, lon)
                else:
                    lat = round(current.lat - deltaYlat, 12)
                    lon = round(current.lon - deltaYlon, 12)
                    newPt = GPScoord(lat, lon)
                chkPts.append(newPt)
                current = newPt
                j += 1
            lat = round(current.lat + deltaXlat, 12)
            lon = round(current.lon + deltaXlon, 12)
            newPt = GPScoord(lat, lon)
            if (i != np.ceil(sideDiv)):
                chkPts.append(newPt)
            current = newPt
            i += 1
            j = 0
        return chkPts