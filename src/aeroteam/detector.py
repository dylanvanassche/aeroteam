#!/usr/bin/python
"""
This module detects geometric shapes in a frame.
Currently, the detector can recognize the following geometric shapes:

- Rectangle
- Arrow

The detector does this by searching for the contours of a shape in the
videostream. As soon as a shape has been found, it will try to detect the
type of the shape. If the shape couldn't be detected it will throw
_UknownShapeException_, in all other cases it returns a list of detected shapes.
"""

from aeroteam.constants import *
from aeroteam.filtering import *
from aeroteam.models import *
import numpy as np
import cv2
import os

# Expose only public classes
__all__ = ["Detector", "UnknownShapeException"]

# https://stackoverflow.com/questions/1319615/proper-way-to-declare-custom-exceptions-in-modern-python
class UnknownShapeException(Exception):
    pass

class Detector(object):
    """
    Creates a Detector instance with the default values defined in the Constants
    module.

    __Parameters (optional)__

    - min_thresh: min pixel value for OpenCV treshold
    - max_thresh: max pixel value for OpenCV treshold
    - blur: OpenCV GaussianBlur value
    - lin_eps: The linearization smoothing epsilon for OpenCV approxPolyDP

    """
    def __init__(self, min_thresh=DETECT_TRESHOLD["min"], \
    max_thresh=DETECT_TRESHOLD["max"], blur=DETECT_BLUR, \
    lin_eps=DETECT_LIN_EPS):
        self.min_threshold = min_thresh
        self.max_threshold = max_thresh
        self.blur = blur
        self.lin_eps = lin_eps
        self.filter = Filter()
        pass

    def __str__(self):
        return "Detector(MIN_TRESH={0}, MAX_THRESH={1}, BLUR={2}, LIN_EPS={3}, FILTER={4})"\
        .format(self.min_threshold, self.max_threshold, self.blur, self.lin_eps, self.filter)

    def find_contours(self, frame):
        """
        This method uses the OpenCV threshold method after converting and
        blurring the color frame into a grayscale frame.
        it's parameters are configured when the Detector instance was created.
        The threshold method is optimized using the
        [Otsu's Binarization](https://docs.opencv.org/trunk/d7/d4d/tutorial_py_thresholding.html).

        If you want to remove small (noisy) contours you can use this while
        looping through then:

	    if cv2.contourArea(c) < 100:
		          continue

        Finding the coordinates of each contour can be done with the help of
        the `imutils` package.
        Use the method `order_points` from the `perspective` submodule together
        with the `cv2.boxPoints()` method. More information can be found [here]
        (https://www.pyimagesearch.com/2016/03/21/ordering-coordinates-clockwise-with-python-and-opencv/)
        and [here](https://www.pyimagesearch.com/2016/04/11/finding-extreme-points-in-contours-with-opencv/)

        __Parameters__

        - frame: a frame in BGR mode

        __Returns__

        - contours: a list of contours retrieved from the frame
        """
        # Convert to grayscale and blur it slightly (denoising)
        # https://docs.opencv.org/3.3.0/d7/d1b/group__imgproc__misc.html#ga397ae87e1288a81d2363b61574eb8cab
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # BORDER_CONTSTANT is used to calculate the pixels outside of the
        # image for blurring, this parameter ignores the calculation since we
        # don't really need it (increases performance)
        # https://docs.opencv.org/3.3.0/d4/d86/group__imgproc__filter.html#gaabe8c836e97159a9193fb0b11ac52cf1
        frame_blur = cv2.GaussianBlur(frame_gray, (self.blur, self.blur), cv2.BORDER_CONSTANT)

        # Treshold the blurred frame binary with Otsu Binarization
        # https://docs.opencv.org/3.3.0/d7/d1b/group__imgproc__misc.html#gae8a4a146d1ca78c626a53577199e9c57
        retval, threshold = cv2.threshold(frame_blur, self.min_threshold, self.max_threshold, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

        # Find the contours of the frame
        # https://docs.opencv.org/3.3.0/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a
        frame2, contours, hierarchy = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        return contours

    def draw_contours(self, contours, frame, color, thickness):
        """
        Draws the given contours on the frame, handy for debugging purposes or
        to give the user some feedback. All the given contours will be drawn on
        the frame.

        __Parameters__

        - contours: a list of contours which will be drawn on the frame
        - frame: a frame in BGR mode
        - color: BGR tupple colors
        - thickness: integer which represent the line tickness of the contours

        __Returns__

        - frame: a frame in BGR mode
        """

        # https://docs.opencv.org/3.3.0/d6/d6e/group__imgproc__draw.html#ga746c0625f1781f1ffc9056259103edbc
        cv2.drawContours(frame, contours, -1, color, thickness)
        return frame

    def find_shape(self, contours, frame, color):
        """
        Guesses based on the received contours which shape the contours are
        representing. The contours are automatically cleaned up by searching for
        the biggest contour on the frame.

        __Parameters__

        - contours: a list of contours which will be drawn on the frame from
        find_contours()
        - frame: an OpenCV compatible frame in BGR mode
        - color: Color of the shape, detected using the filter masks in Filter()

        __Returns__

        - Shape: Rectangle, Arrow, ... shape model

        __Raises__

        - UnknownShapeException: if the shape is unknown an
        UnknownShapeException is raised. The current frame and it's contours
        will be drawn on the frame and saved in the working directory for
        debugging purposes.
        """

        # Filter out unwanted contours
        main_contour = self.filter.remove_false_contours(contours, frame)

        if(len(main_contour) == 0):
            raise UnknownShapeException("Frame is empty after denoising the contours!")

        # Find the contour perimeter (closed=True) and find epsilon
        # https://docs.opencv.org/3.3.0/d3/dc0/group__imgproc__shape.html#ga8d26483c636be6b35c3ec6335798a47c
        perimeter = cv2.arcLength(main_contour, True)
        epsilon = self.lin_eps*perimeter

        # Linearize the contours (closed = True)
        # https://docs.opencv.org/3.3.0/d3/dc0/group__imgproc__shape.html#ga0012a5fdaea70b8a9970165d98722b4c
        approx = cv2.approxPolyDP(main_contour, epsilon, True)

        #print("Contours# = {0}".format(str(len(approx))))

        # Create a Rectangle object with the given points and return it.
        if len(approx) in RECTANGLE_POINTS:
            rectangle = Rectangle(approx, frame, color)
            return rectangle

        # Create an Arrow object with the given points and return it.
        elif len(approx) in ARROW_POINTS:
            arrow = Arrow(approx, frame, color)
            return arrow

        # Create a Cross object with the given points and return it.
        elif len(approx) in CROSS_POINTS:
            cross = Cross(approx, frame, color)
            return cross

        # Shape is unknown, raise error and save the frame for debugging.
        else:
            raise UnknownShapeException("Shape couldn't be determined!")
