#!/usr/bin/python

"""
The Drone class represents the drone and provides several methods to control the drone.
"""

from aeroteam.constants import *
from dronekit import connect, VehicleMode, LocationGlobalRelative, APIException
from pymavlink import mavutil # Needed for command message definitions
import time
import math
import numpy as np

# Expose only public classes
__all__ = ["Drone", "CommandException"]

class CommandException(Exception):
    """
    Operating the drone in a different mode then `GUIDED` will raise a
    CommandException.
    """
    pass

class Drone(object):
    """
    Creates a Drone instance with a given name and a connection.

    __Parameters__

    - name: string to represent the name of the drone
    - connection: a connection string for DroneKit
    """
    def __init__(self, name, connection):
        self.name = name
        self.connection = connection
        self.vehicle = None # DroneKit needs to be added

    def __str__(self):
        return "Drone(NAME={0}, CONNECTION={1})".format(self.name, self.connection)

    def setup(self):
        """
        Connects to the drone using a DroneKit connection, it can take
        up to 30 seconds until the connection is ready. When a timeout occurs,
        an CommandException is raised.

        __Raises__

        - CommandException: connection timeout
        """
        print("Setting up a connection...")
        try:
            self.vehicle = connect(self.connection, wait_ready=True)
        except APIException as error: # pragma: no cover
            raise CommandException(error)

    def arm(self, state):
        """
        Arms the drone and makes it ready for take off.

        __Parameters__

        - state: boolean to arm (True) or disarm (False) the drone.
        """
        # Wait until arming is possible
        while(not self.vehicle.is_armable):
            print("Waiting for the flightcontroller to initialise...")
            time.sleep(1)

        # Arm the drone
        print("Arming motors NOW!")
        self.vehicle.armed = state

        # Wait until arming procedure succeed
        while not self.vehicle.armed:
            print ("Waiting until arming is completed...")
            time.sleep(1)

        # Before we can take off we need a 3D GPS fix
        print("GPS fix status: {0}".format(self.vehicle.gps_0.fix_type))
        while self.vehicle.gps_0.fix_type < 3: # pragma: no cover
            print ("Waiting for GPS 3D fix... {0}".format(self.vehicle.gps_0.fix_type))
            time.sleep(1)

        # Make sure that vehicle commands are flushed to the drone
        self.vehicle.flush()

    def set_mode(self, mode):
        """
        Sets the operation mode for the drone. Several modes are available from
        the Python Dronekit module:

        - RTL: Return To Launch
        - GUIDED: Follows the waypoints
        - AUTO: Executes a given mission

        __Parameters__

        - mode: a string to represent the mode (`RTL`, `GUIDED`, `AUTO`)
        """
        self.vehicle.mode = VehicleMode(mode)

    def take_off(self, altitude):
        """
        Starts the drone and let it reach it's altitude.

        _Warning_! The drone mode must be set to `GUIDED` before calling this
        method.

        __Parameters__

        - altitude: the altitude that needs to be reached when taking off.
        """
        # Command the drone to reach the given altitude
        print("Taking off!")
        self.vehicle.simple_takeoff(altitude)

        # Only perform this action when we're running in GUIDED mode
        while(True):
            print("Current altitude: {0} m".format(str(self.get_location().alt)))

            # Break when we reached almost our altitude
            # http://python.dronekit.io/examples/simple_goto.html
            if(np.isclose(self.get_location().alt, altitude, atol=DRONE_MAX_DELTA_ALTITUDE)):
                print("Reached target altitude: {0} m (real: {1} m) OK\n".format(altitude, self.get_location().alt))
                break

            # Update every 2 seconds
            time.sleep(2)

    def _get_distance_metres(self, location1, location2):
        """
        Returns the ground distance in metres between two LocationGlobal objects.
        This method uses Numpy to improve accuracy and speed when working with
        very small distances!

        _Source_: [ArduPilot test code](https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py)

        _Warning_! The method is an approximation, and will not be accurate
        over large distances.

        __Parameters__

        - location1: LocationGlobal object 1
        - location2: LocationGlobal object 2
        """
        dlat = np.float64(location2.lat) - np.float64(location1.lat)
        dlong = np.float64(location2.lon) - np.float64(location1.lon)
        distance = np.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5
        return distance

    def get_location(self):
        """
        Retrieves the current location of the drone from Dronekit.

        __Returns__

        - location: Dronekit LocationGlobalRelative object
        """
        return self.vehicle.location.global_relative_frame

    def get_heading(self):
        """
        Retrieves the current heading of the drone from Dronekit where
        North = 0 degrees (integer).

        __Returns__

        - heading: the current heading in degrees (integer)
        """
        return self.vehicle.heading

    def go_to_gps(self, target, blocking=True):
        """
        Go to a specified GPS coordinate. The altitude is relative to the home
        location of the drone and not to the mean sea level!

        _Warning_! The drone mode must be set to `GUIDED` before calling this
        method.

        This method is based on the [Python Dronekit examples](http://python.dronekit.io/examples/guided-set-speed-yaw-demo.html#example-guided-mode-setting-speed-yaw).

        __Parameters__

        - target: LocationGlobalRelative object with the target location
        - blocking: if True, this method will block until the drone reached it's
        target location (default)

        __Raises__

        - CommandException: Commanding the drone in a different mode then
        `GUIDED` isn't allowed.
        """

        # Only perform this action when we're running in GUIDED mode
        if self.vehicle.mode.name != "GUIDED":
            raise CommandException("Drone isn't operating in GUIDED mode!")

        # Create the target location and calculate the distance to the target
        # location
        target_distance = self._get_distance_metres(self.get_location(), target)
        print("Current location: {0}".format(self.get_location()))
        print("Target location: {0}, {1} metres away".format(target, target_distance))

        # Command the drone to fly to the given target location
        self.vehicle.simple_goto(target)

        # If required, block until drone reaches it's location
        while(blocking == True):
            # Find the remaining distance to the target location
            remaining_distance = self._get_distance_metres(self.get_location(), target)

            # When we're almost at the target location, break
            # GPS accuracy is +- 3 metres tops, depending on the distance we use
            # DRONE_MAX_PERCENTAGE_DISTANCE of the target distance or a minimum of DRONE_MAX_DELTA_DISTANCE.
            # http://python.dronekit.io/examples/guided-set-speed-yaw-demo.html
            # Verify our position
            if remaining_distance <= max(target_distance*DRONE_MAX_PERCENTAGE_DISTANCE, DRONE_MAX_DELTA_DISTANCE):
                print("Reached target location: lat={0},lon={1}".format(target.lat, target.lon))

                # Verify our altitude
                if(np.isclose(self.get_location().alt, target.alt, atol=DRONE_MAX_DELTA_ALTITUDE)):
                    print("Reached target altitude: {0} m (real: {1} m) OK\n".format(target.alt, self.get_location().alt))
                    break
                else:
                    print("Waiting to reach target altitude...")
            else:
                print("Distance to target: {0} metres away".format(remaining_distance))

            # Update every 2 seconds
            time.sleep(2)

    def set_airspeed(self, speed):
        """
        Sets the airspeed of the drone. This isn't necessary since the Ardupilot
        has preconfigured default settings.

        __Parameters__

        - speed: the airpseed in m/s of the drone
        """
        self.vehicle.airspeed = speed

    def set_groundspeed(self, speed):
        """
        Sets the groundspeed of the drone. This isn't necessary since the
        Ardupilot has preconfigured default settings.

        __Parameters__

        - speed: the groundspeed in m/s of the drone
        """
        self.vehicle.groundspeed = speed

    def land(self):
        """
        Land on the current location.
        """
        self.set_mode("LAND")
        while(self.get_location().alt > DRONE_MAX_LANDING_ALTITUDE):
            print("Landing... current altitude: {0} metres".format(self.get_location().alt))
            time.sleep(2)
        print("Landing complete! current altitude: {0} metres\n".format(self.get_location().alt))

    def stop(self):
        """
        Closes the vehicle object as documented in the Python Dronekit
        documentation. You can find more information about this best practices
        [here](http://python.dronekit.io/develop/best_practice.html?highlight=exit).
        """
        # Safety first
        self.vehicle.armed = False

        # Close the vehicle connection
        self.vehicle.close()
        print("Drone shutdown complete")

    def drop_package(self, altitude):
        """
        Drop the package on the current location.
        The altitude is relative to the home location.

        This method uses a custom crafted Mavlink message since controlling a
        servo output directly isn't supported by default in DroneKit.
        You can find an example [here](https://stackoverflow.com/questions/36636073/is-it-possible-to-control-an-actuator-using-dronekit-python).

        __Parameters__

        - altitude: the altitude relative to the home location when dropping
        the package
        """
        # Retrieve the current location of the drone
        print("Flying to dropping location")
        current_location = self.get_location()
        old_altitude = current_location.alt

        # Altitude is relative to the home position, landing altitude = 0
        drop_location = LocationGlobalRelative(current_location.lat, current_location.lon, altitude)

        # Move to dropping location
        self.go_to_gps(drop_location)

        # Prepare MAVLink message to open the bomber system
        msg_open = self.vehicle.message_factory.command_long_encode(
        0, 0,   # target_system, target_component
        mavutil.mavlink.MAV_CMD_DO_SET_SERVO,   # command
        0,  # confirmation
        DRONE_DROP_SERVO,  # servo number
        DRONE_DROP_OPEN,   # servo position between 1000 and 2000
        0, 0, 0, 0, 0)  # param 3 ~ 7 not used

        # Prepare MAVLink message to close the bomber system
        msg_close = self.vehicle.message_factory.command_long_encode(
        0, 0,   # target_system, target_component
        mavutil.mavlink.MAV_CMD_DO_SET_SERVO,   # command
        0,  # confirmation
        DRONE_DROP_SERVO,  # servo number
        DRONE_DROP_CLOSE,   # servo position between 1000 and 2000
        0, 0, 0, 0, 0)  # param 3 ~ 7 not used

        # Open the bomber system
        print("Dropping NOW!")
        self.vehicle.send_mavlink(msg_open)

        # Wait until dropping is done
        time.sleep(DRONE_DROP_WAIT)

        # Close the bomber system
        self.vehicle.send_mavlink(msg_close)
        print("Package dropped succesfully")

        # Retrieve the current location of the drone
        print("Returning to previous altitude for flying")
        current_location = self.get_location()

        # Altitude is relative to the home position, landing altitude = 0
        fly_location = LocationGlobalRelative(current_location.lat, current_location.lon, old_altitude)

        # Return to previous altitude
        self.go_to_gps(fly_location)

    def add_observer(self, attribute, observer):
        """
        Connects a callback function to a specific attribute of the DroneKit
        Vehicle object. You can find more information about this
        [here](http://python.dronekit.io/1.5.0/automodule.html).

        __Parameters__

        - observer: Python function used as callback
        """
        self.vehicle.add_attribute_listener(attribute, observer)

    def remove_observer(self, attribute, observer):
        """
        Disconnects a callback function to a specific attribute of the DroneKit
        Vehicle object. You can find more information about this
        [here](http://python.dronekit.io/1.5.0/automodule.html).

        __Parameters__

        - observer: Python function used as callback
        """
        self.vehicle.remove_attribute_listener(attribute, observer)
