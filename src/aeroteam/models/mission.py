#!/usr/bin/python

"""
The Mission class is an abstract class that represents a mission and provides
methods to carry out the mission.
"""

import time
import cv2
import math
from abc import ABCMeta, abstractmethod
from shapely.geometry import Point
from shapely.geometry import LineString
from collections import namedtuple
from dronekit import LocationGlobalRelative
from Queue import Empty
from aeroteam.constants import *
from aeroteam.processor import *
from aeroteam.models.shapes import *
from aeroteam.models.colors import *
from aeroteam.tools import *

# Expose only the public classes
__all__ = ["Mission1", "Mission2", "Mission3", "MissionFailureException"]

# structure to save detected shapes
shapestruct = namedtuple("Shape", "name center direction location")

class MissionFailureException(Exception):
    """
    When a mission fails a MissionFailureException will be raised.
    """
    pass

class Mission(object):
    # Use abc module to make Mission class abstract
    __metaclass__ = ABCMeta

    """
    Creates a Mission instance with a given drone, videostream filter,
    shape detector and Darknet library

    __Parameters__

    - drone: DroneKit Drone instance
    - processor: OpenCV Processor instance
    """
    def __init__(self, drone, camera):
        self.drone = drone
        self.camera = camera
        self.processor = None
        self.shapes = []
        self.current_step = 0
        self._has_bomb = True
        self.has_bomb_requests = 0 # REMOVE AFTER DEBUG

    def next_step(self, step):
        """
        Increments the `current_step` and displays this to the console.

        __Parameters__

        - step: he name of the current step (string)
        """
        self.current_step = self.current_step + 1
        print("Step {0}: {1}".format(self.current_step, step))

    def has_bomb(self):
        return self._has_bomb

    def verify_target(self, shape, color):
        """
        Checks if we found our bomb target and drops the bomb if needed.

        _Warning_! The drone will NOT keep it's course after calling this method.

        __Parameters__

        - shape: shape of the bomb target
        - color: color of the bomb target

        __Returns__

        - found: `True` when bomb target was found, `False` when not
        """
        # Bomb already dropped
        if not self.has_bomb():
            return False

        # Searching for target
        for detection in self.shapes:
            # Verify the target
            if isinstance(detection["shape"], shape) and detection["shape"].color == color:
                print("Bomb target found!")
                self.drone.drop_package(DEFAULT_DROP_ALTITUDE)
                self._has_bomb = False
                return True

        # Target not found
        return False

    def compare_points(self, co1, co2):
        """
        Compares two gps coordinates and determines if they are close enough to
        be considered equal

        __Parameters__

        - co1: first coordinate to be compared
        - co2: second coordinate to be compared

        __Returns__

        - equal: `True` when within range, `False` when not
        """

        lat_dif = co1.lat-co2.lat
        lon_dif = co1.lon-co2.lon

        if ((lat_dif)**2+(lon_dif)**2)**(0.5) < 1.0*10.0**(-5.0):
            return True
        else:
            return False


class Mission1(Mission):
    """
    Mission 1: Choose the good target
    See the `run` for more information about this mission.

    __Parameters__

    - drone: DroneKit Drone instance
    - camera: String for camera location path
    """
    def __init__(self, drone, camera):
        super(Mission1, self).__init__(drone, camera)

    def run(self, startpoint, endpoint, shape, color):
        """
        Starts the 1st mission: Choose the good target.

        __Steps__

        1. Automated take off.
        2. Fly to the startpoint.
        3. Set course for the endpoint.
        4. Start the detections processor while flying to the endpoint.
        5. Drop the bomb when the right target was found.
        6. Fly to the endpoint.
        7. Land automated and clean.

        __Parameters__

        - startpoint: Shapely point as the drone's takeoff location
        - endpoint: Shapely point as the drone's land location
        - shape: Type of shape (string) as bomb target
        - color: Color of the shape (string) as bomb target
        """
        # Step 1: Execute automated take off
        self.next_step("Taking off")
        self.drone.arm(True)
        self.drone.set_mode("GUIDED")
        self.drone.set_airspeed(DEFAULT_AIRSPEED)
        self.drone.set_groundspeed(DEFAULT_GROUNDSPEED)
        self.drone.take_off(DEFAULT_ALTITUDE)

        # Update the GPS points with the right altitude
        startpoint.alt = self.drone.get_location().alt
        endpoint.alt = self.drone.get_location().alt

        # Step 2: Fly to the startpoint
        self.next_step("Go to GPS location (home location)")
        self.drone.go_to_gps(startpoint)

        # Step 3: Set course to the landing point
        self.next_step("Setting course to GPS location (landing location)")
        self.drone.go_to_gps(endpoint, blocking=False)

        # Step 4: Prepare the detection processor
        self.next_step("Running detection algorithms")
        # Restart the video processor to make sure we are detecting only the correct area
        self.processor = Processor(self.camera, self.drone)

        # Step 5: Dropping on the bomb target
        self.next_step("Dropping bomb on target (if found)")
        while(True):
            try:
                self.shapes = self.processor.get_detections(timeout=1)

                # Loop through the detections if we haven't found the bomb target yet
                print("Searching for target...")
                if self.verify_target(Rectangle, Color.RED):
                    print("Target found and bomb dropped succesfully!")
                    break

            # No detections available...
            except Empty:
                pass

        if self.has_bomb():
            raise MissionFailureException("Bomb target wasn't found!")

        # Step 6: Flying to the landing location
        self.next_step("Go to GPS location (landing location)")
        self.drone.go_to_gps(endpoint)

        # Step 7: Land automated + cleanup
        self.next_step("Landing")
        self.drone.land()
        self.processor.stop()

class Mission2(Mission):
    """
    Mission 2: Triangulate my location
    See the `run` for more information about this mission.

    __Parameters__

    - drone: DroneKit Drone instance
    - camera: String for camera location path
    """

    def __init__(self, drone, camera):
        super(Mission2, self).__init__(drone, camera)

    def run(self, startpoint, waypoint):
        """
        Starts the 2nd mission: Triangulate my location.

        __Steps__

        1. Automated take off.
        2. Fly to the startpoint.
        3. Set course for the waypoint.
        4. Start the detections processor while flying to the waypoint.
        5. Fly to the endpoint.
        6. Land automated and clean.

        __Parameters__

        - waypoint: Shapely point as the drone's dummy destination
        """

        tools = Tools()
        arrows = []

        # Step 1: Execute automated take off
        self.next_step("Taking off")
        self.drone.arm(True)
        self.drone.set_mode("GUIDED")
        self.drone.set_airspeed(DEFAULT_AIRSPEED)
        self.drone.set_groundspeed(DEFAULT_GROUNDSPEED)
        self.drone.take_off(DEFAULT_ALTITUDE)

        # Step 2: Fly to the startpoint
        self.next_step("Go to GPS location (home location)")
        self.drone.go_to_gps(startpoint)

        # Step 3: Set course to the waypoint
        self.next_step("Setting course to GPS waypoint")
        self.drone.go_to_gps(waypoint, blocking=False)

        # Step 4: Detecting shapes
        self.next_step("Running detection algorithms(first arrow)")
        self.processor = Processor(self.camera, self.drone)
        number_of_shapes = 0
        while (len(self.shapes) < 2) and \
        not(self.compare_points(self.drone.get_location(), waypoint)):
            try:
                detections = self.processor.get_detections(timeout=1)
                if len(detections) > number_of_shapes:
                    new_shape = detections[number_of_shapes-1]
                    if isinstance(new_shape["shape"], Arrow):
                        self.shapes.append(new_shape)
                        angle, tip, back, center = new_shape["shape"].find_direction()
                        location = self.drone.get_location()
                        location_gps = tools.waypoint(location.lat, location.lon, location.alt)
                        size = self.processor.get_current_frame(1, block=False).shape
                        width = size[1]
                        heigth = size[0]
                        heading = self.drone.get_heading()
                        tip_gps = tools.img2GPS(tip["point"], location_gps, width, heigth, heading)
                        back_gps = tools.img2GPS(back["point"], location_gps, width, heigth, heading)
                        arrows.append(tools.lineString2P(back_gps, tip_gps))
                        number_of_shapes += 1

                        if len(self.shapes) == 1:
                            self.next_step("Running detection algorithms(second arrow)")
            except Empty:
                pass

        # Step 5: Flying towards endpoint
        if  len(self.shapes) != 2:
            self.next_step("Not enough arrows detected, mission failed")

        else:
            self.next_step("two arrows detected, calculating landing location")
            lijn1 = arrows[0]
            lijn2 = arrows[1]
            target = tools.findIntersect(lijn1, lijn2)
            target_gps = LocationGlobalRelative(target.lat, target.lon, 30)
            self.drone.go_to_gps(target_gps)

        # Step 6: Land automated + cleanup
        self.next_step("Landing")
        self.drone.land()

class Mission3(Mission):
    """
    Mission 3: Search and rescue.
    See the `run` for more information about this mission.

    __Parameters__

    - drone: DroneKit Drone instance
    - camera: String for camera location path
    """

    def __init__(self, drone, camera):
        super(Mission3, self).__init__(drone, camera)

    def run(self, p1, p2, p3, p4, shape, color):
        """
        Starts the 2nd mission: Triangulate my location.

        __Steps__

        1. Automated take off.
        2. Fly to the startpoint.
        3. Create scan pattern.
        4. Follow scan pattern and detect shapes.
        5. Flying towards endpoint and determining mission success.
        6. Land automated + cleanup.
        """

        tools = Tools()

        # Step 1: Execute automated take off
        self.next_step("Taking off")
        self.drone.arm(True)
        self.drone.set_mode("GUIDED")
        self.drone.set_airspeed(DEFAULT_AIRSPEED)
        self.drone.set_groundspeed(DEFAULT_GROUNDSPEED)
        self.drone.take_off(DEFAULT_ALTITUDE)

        # Update the GPS points with the right altitude
        startpoint = p1
        endpoint = p1
        startpoint.alt = self.drone.get_location().alt
        endpoint.alt = self.drone.get_location().alt

        # Step 2: Fly to the startpoint
        self.next_step("Go to GPS location (home location)")
        self.drone.go_to_gps(startpoint)

        # Step 3: Create scan pattern
        self.next_step("creating scan pattern")
        tools = Tools()
        number_of_arrows = 0
        number_of_shapes = 0
        points = tools.scanPattern(p1, p2, p3, p4)
        arrows = []

        # Step 4: Follow scan pattern and detect shapes
        self.next_step("Running detection algorithms, and following scan pattern")
        self.processor = Processor(self.camera, self.drone)
        for p in points:
            p = LocationGlobalRelative(p.lat, p.lon, self.drone.get_location().alt)
            self.drone.go_to_gps(p, blocking=False)
            while not self.compare_points(self.drone.get_location(), p):
                try:
                    detections = self.processor.get_detections(timeout=1)
                    if len(detections) > number_of_shapes:
                        number_of_shapes += 1
                        new_shape = detections[number_of_shapes-1]
                        self.shapes.append(new_shape)
                        if isinstance(new_shape["shape"], Arrow):
                            angle, tip, back, center = new_shape["shape"].find_direction()
                            location = self.drone.get_location()
                            location_gps = tools.waypoint(location.lat, location.lon, location.alt)
                            size = self.processor.get_current_frame(1, block=False).shape
                            width = size[1]
                            heigth = size[0]
                            heading = self.drone.get_heading()
                            tip_gps = tools.img2GPS(tip["point"], location_gps, width, heigth, heading)
                            back_gps = tools.img2GPS(back["point"], location_gps, width, heigth, heading)
                            arrows.append(tools.lineString2P(back_gps, tip_gps))
                            number_of_arrows += 1
                        if self.verify_target(Rectangle, Color.RED):
                            print("Target found and bomb dropped succesfully!")
                            # return to course after verify_target
                        self.drone.go_to_gps(p, blocking=False)
                except Empty:
                    pass

        # Step 5: Flying towards endpoint and determining mission success
        if  number_of_arrows < 2:
            self.next_step("Not enough arrows detected, mission failed")

        else:
            self.next_step("two arrows detected, calculating landing location")
            lijn1 = arrows[0]
            lijn2 = arrows[1]
            endpoint = tools.findIntersect(lijn1, lijn2)
            endpoint = LocationGlobalRelative(endpoint.lat, endpoint.lon, 30)

        if self.has_bomb():
            self.next_step("Bomb not dropped, mission failed")

        self.drone.go_to_gps(endpoint)

        # Step 6: Land automated + cleanup
        self.next_step("Landing")
        self.drone.land()



class MissionUnittest(Mission):
    """
    Mission Unittest: tests every item that can occur in a mission.

    __Steps__

    1. Take off
    2. Go to a given GPS location
    3. Execute a search pattern
    4. While running the search pattern, recognize 6 different shapes
    (rectangle, arrow, cross in yellow and red colors)
    5. Drop the bomb on the given target shape
    6. Fly in a straight line to the landing location

    __Parameters__

    - drone: DroneKit Drone instance
    - processor: OpenCV Processor instance
    """

    def __init__(self, drone, processor):
        super(MissionUnittest, self).__init__(drone, processor)

    def run(self, startpoint, endpoint, p1, p2, p3, p4, shape, color):
        """
        Executes the mission, this method can only run when all needed
        parameters are configured!

        _Warning_: Not catching a MissionFailureException will keep the drone
        in the air! You should call `drone.land()` when a
        MissionFailureException is raised!

        __Parameters__

        - startpoint: GPS location of the home location (LocationGlobalRelative)
        - endpoint: GPS location of the landing location (LocationGlobalRelative)
        - p1: left-bottom GPS location of the search area (LocationGlobalRelative)
        - p2: left-top GPS location of the search area (LocationGlobalRelative)
        - p3: right-top GPS location of the search area (LocationGlobalRelative)
        - p4: right-bottom GPS location of the search area (LocationGlobalRelative)
        - shape: Type of shape as the bomb target (Shape)
        - color: color of the shape as the bomb target (Color enum)

        __Raises__

        - MissionFailureException: When the bomb target couldn't be retrieved
        in the search area, a MissionFailureException is raised
        """
        ########################################################################
        # WAITING FOR SEARCH PATTERN TODO
        #target_location = None
        #search_area = []

        # Step 1: Execute automated take off
        #self.next_step("Taking off")
        #self.drone.arm(True)
        #self.drone.set_mode("GUIDED")
        #self.drone.set_airspeed(DEFAULT_AIRSPEED)
        #self.drone.set_groundspeed(DEFAULT_GROUNDSPEED)
        #self.drone.take_off(DEFAULT_ALTITUDE)

        # Step 2: Fly to given GPS location
        #self.next_step("Go to GPS location (home location)")
        #self.drone.go_to_gps(startpoint)

        # Step 3: Execute search pattern
        # Call Tools search pattern generator
        # Generate a list of LocationGlobalRelative points
        #self.next_step("Generating search pattern")

        # Step 4: Collect all possible target shapes
        # Loop through all the LocationGlobalRelative points with a drone.go_to_gps
        # Keep the Processor running for detections in the mean time
        #self.next_step("Running detection algorithms on search pattern")
        # Restart the video processor to make sure we are detecting only the search pattern area.
        #self.processor.restart()

        #for location in search_area:
            # Fly over the search area
            #print("Searching area: {0}".format(location))
            #self.drone.go_to_gps(location)

            # Keep the camera steady for a couple of seconds
            #time.sleep(2)

            # Retrieve the detections
            #try:
                #detections = self.processor.get_detections(timeout=3)

                # Loop through the detections if we haven't found the bomb target yet
                #if target_location is None:
                    #for d in detections:
                        # Check if we found the bomb target
                        #if isinstance(d, shape) and d.color == color:
                            #print("Bomb target detected!")
                            #target_location = current_location

            # No detections available...
            #except Empty:
                #pass

        # Step 5: Drop the bomb on the target shape
        #self.next_step("Dropping bomb on given target")
        # If the target_location is None then it was not found in the search area
        #if target_location is not None:
            #self.drone.go_to_gps(target_location)
            #self.drone.drop_package(DEFAULT_DROP_ALTITUDE)
        #else:
            #raise MissionFailureException("Bomb target ({0} {1}) couldn't be detected".format(color, shape))

        # Step 6: Fly to the landing location
        #self.next_step("Go to GPS location (landing location)")
        #self.drone.go_to_gps(endpoint)

        # Step 7: Execute automated landing
        #self.next_step("Landing")
        #self.drone.land()
        ########################################################################
