#!/usr/bin/python
"""
All colors that are supported in the aeroteam module for detection.
A big thanks to the [Python3 Enum example](https://docs.python.org/3/library/enum.html)!
"""

from enum import Enum

# Expose only public classes
__all__ = ["Color", "UnknownColorException"]

class UnknownColorException(Exception):
    """
    Retrieving the color of the shape might fail, an UnknownColorException is raised.
    """
    pass

class Color(Enum):
    """
    Enumeration which contains all the supported colors for object detection.
    """
    RED = 1
    YELLOW = 2
    UNKNOWN = 3
