#!/usr/bin/python
"""
All shapes that are supported in the aeroteam module.
"""
import cv2
import math
import numpy as np
from shapely.geometry import Point, LineString, Polygon
from aeroteam.constants import *

# Only expose the subclasses since we only support those
__all__ = ["Shape", "Rectangle", "Cross", "Arrow", "InvalidShapeException"]

# https://stackoverflow.com/questions/1319615/proper-way-to-declare-custom-exceptions-in-modern-python
class InvalidShapeException(Exception):
    """
    Calculating shape attributes might fail. If raised, the shape must be thrown
    away.
    """
    pass

class Shape(object):
    """
    Shape baseclass for creating all supported shapes.

	__Parameters__

    - contours: Numpy array of OpenCV contours
    - frame: an OpenCV compatible videoframe in BGR mode
    - color: a Color enum to represent the color of the detected object.

    """
    def __init__(self, contours, frame, color):
        self.contours = contours
        self.frame = frame
        self.color = color

        # Convert openCV points to Shapely Points
        points = list(map(lambda x: Point(x[0]), contours))
        self.points = points

    def find_center(self):
        """
        Finds the center of the shape using cv2.moments() more information about
		this method can be found [here](https://docs.opencv.org/3.3.0/d3/dc0/group__imgproc__shape.html#ga556a180f43cab22649c23ada36a8a139).

        _Warning!_ This method returns the wrong center for a fitted line in the
        Arrow class. If you need the center of the fitted line,
        use `self.find_center_fitline(tip_back_points)`
        """
        moment = cv2.moments(self.contours)
        x = int(moment["m10"] / moment["m00"])
        y = int(moment["m01"] / moment["m00"])
        center = Point(x, y)
        #print("Center: {0}".format(center))
        cv2.circle(self.frame,(int(center.x), int(center.y)), POINT_SIZE, PURPLE, -1)
        return center

    def __eq__(self, other):
        """
        Checks if 2 shapes are equal to each other.
        Usage: `a == b`

        __Parameters__

        - other: Python object
        """
        return self.color == other.color and type(self) == type(other)

# Subclasses
class Rectangle(Shape):
    """
    Rectangle class from superclass Shape.
    Dassault rectangles are 20 cm x 100 cm.

    __Parameters__

    - points: Numpy array of OpenCV contours
    - frame: an OpenCV compatible videoframe in BGR mode
    - color: a Color enum to represent the color of the detected object.

    __Raises__

    - InvalidShapeException: if the shape is invalid.
    """
    def __init__(self, contours, frame, color):
        super(Rectangle, self).__init__(contours, frame, color)
        #self._is_valid()

    def __str__(self):
        return "Rectangle(COLOR={0})".format(self.color)


class Cross(Shape):
    """
    Cross class from superclass Shape.
    Dassault crosses are 100 cm x 100 cm.

    __Parameters__

    - points: Numpy array of OpenCV contours
    - frame: an OpenCV compatible videoframe in BGR mode
    - color: a Color enum to represent the color of the detected object.

    __Raises__

    - InvalidShapeException: if the shape is invalid.
    """
    def __init__(self, contours, frame, color):
        super(Cross, self).__init__(contours, frame, color)
        #self._is_valid()

    def __str__(self):
        return "Cross(COLOR={0})".format(self.color)

class Arrow(Shape):
    """
    Arrow class from superclass Shape.
    Dassault arrows are 100 cm long, 20 cm width (back).

    __Parameters__

    - points: Numpy array of OpenCV contours
    - frame: an OpenCV compatible videoframe in BGR mode
    - color: a Color enum to represent the color of the detected object.

    """
    def __init__(self, contours, frame, color):
        super(Arrow, self).__init__(contours, frame, color)
        self.find_direction() # Check if valid by calculating the direction

    def __str__(self):
        return "Arrow(COLOR={0})".format(self.color)

    def _sort_points_on_distance(self, target):
        """
        Sorts the points of the arrow on distance from a certain target.

    	__Parameters__

        - target: Shapely Point
        """
        # Calculating distances with Shapely
        distance_list = []
        for point in self.points:
            data = { "distance": target.distance(point), "point": point }
            distance_list.append(data)

        # Sort on distance and return the sorted list
        return sorted(distance_list, key=lambda x: x["distance"])

    def _total_distance(self, target):
        """
        Calculates the total distance between the target and the points of the
        arrow. This includes it's own point of course, however this has no
        influence on the total sum since it's distance is 0.

    	__Parameters__

        - target: Shapely Point

        __Returns__

        - int: total sum of the distances
        """
        # Map distance function over all points
        total = list(map(lambda x: target.distance(x), self.points))

        # sum() lacks accuracy when working with floats
        # https://docs.python.org/3/library/math.html#math.fsum
        return math.fsum(total)

    def find_center_fitline(self, tip_back_points):
        centerX = (tip_back_points[0]["point"].x + tip_back_points[1]["point"].x)/2
        centerY = (tip_back_points[0]["point"].y + tip_back_points[1]["point"].y)/2
        center = Point(centerX, centerY)
        return center

    def find_direction(self):
        """
        Tries to find the direction and the angle of the arrow referenced to the
        X axis using a fitting line and an intelligent algorithm to find the
        tip of the arrow.

        The angle can be used as the magnitude to extrapolate in the right
        direction. The magnitude is positive between [0.0, 180.0[ and negative
        between [180.0, 360.0[.

        __Returns__

        - angle: float [0.0, 360.0[ which is the difference in degrees between
        the X axis and the direction of the arrow tip. 360.0 degrees is
        converted to 0.0 degrees since they are the same.
        - tip: Shapely Point representing the tip of the arrow in image coordinates.
        - back: Shapely Point representing the back of the arrow in image coordinates.
        - center: Shapely Point representing the center of the arrow in image coordinates.

        """
        # Find the size of the frame
        xSize, ySize = self.frame.shape[:2]

        # Calculate the fitting line
        # Huber Robust Regression algorithm, least square method is faster but
        # less accurate
        # https://en.wikipedia.org/wiki/Robust_regression
		# https://docs.opencv.org/trunk/d3/dc0/group__imgproc__shape.html#gaf849da1fdafa67ee84b1e9a23b93f91f
        # https://stackoverflow.com/questions/2750867/draw-fitted-line-opencv
        # (x,y) = (pointX,pointY) + t*(vectorX,vectorY)
        # t = [-infinity, +infinity]
        [vectorX, vectorY, pointX, pointY] = cv2.fitLine(self.contours, \
        cv2.DIST_HUBER, 0, 0.01, 0.01) # TODO create constant

        # Coordinate system is rotated (-90 deg): *(-1)
        leftY = int((-pointX*vectorY/vectorX) + pointY)
        rightY = int(((ySize-pointX)*vectorY/vectorX) + pointY)

        # Convert to Shapely points
        p1 = Point(ySize-1, rightY)
        p2 = Point(0, leftY)
        fitted_line = LineString([p1, p2])

        # Calculate the distance between the fitted line and the points list
        # The 2 most closed points are the back of the arrow and the arrow tip
        sorted_points = self._sort_points_on_distance(fitted_line)
        tip_back_points = sorted_points[:2]
        other_points = sorted_points[2:]

        # Check if arrow is valid
        #self._is_valid(tip_back_points, other_points)

        # Find the center of the object in the frame to place our origin of the
        # coordinate system
        center = self.find_center_fitline(tip_back_points)

        # Draw everything on the frame for debugging
        cv2.line(self.frame, (ySize-1, rightY), (0, leftY), BLUE, LINE_SIZE)
        cv2.circle(self.frame,(pointX, pointY), POINT_SIZE, YELLOW, -1)
        cv2.circle(self.frame,(int(p1.x), int(p1.y)), POINT_SIZE, YELLOW, -1)
        cv2.circle(self.frame,(int(p2.x), int(p2.y)), POINT_SIZE, YELLOW, -1)
        cv2.circle(self.frame,(int(center.x), int(center.y)), POINT_SIZE,\
        PURPLE, -1)

        # Filter the arrow tip out
        # Distance between the arrow tip and the other points is lower then the
        # back point
        distance_list = []
        for point in tip_back_points:
            data = { "total": self._total_distance(point["point"]), \
            "point": point }
            distance_list.append(data)
        tip = sorted(distance_list, key=lambda x: x["total"])[0]["point"]
        back = sorted(distance_list, key=lambda x: x["total"])[1]["point"]

        # Calculate the angle between the tip and the X axis
        # Translate the tip point with the center point as origin
        # For more information see Analyse 1, Gorik De Samblanx, 2015-2016,
        # Acco, p20
        distance_tip_center = center.distance(tip["point"])
        angle = math.acos((tip["point"].x - center.x)/distance_tip_center)
        angle = math.degrees(angle)

        # Select the right angle since the cosinus has 2 angles
        # (opposite angles) for the same value!
        # Y axis is inverted (>)!
        # For more information see Analyse 1, Gorik De Samblanx, 2015-2016,
        # Acco, p13
        if(tip["point"].y > center.y):
            angle = -angle

        # Limit the angle between [0.0, 360.0] degrees
        while(angle < 0.0):
            angle = angle + 360.0

        while(angle >= 360.0):
            angle = angle - 360.0

        #print("ANGLE=" + str(angle))
        return angle, tip_back_points[0], tip_back_points[1], center
