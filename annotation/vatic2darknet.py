#!/usr/bin/python
"""
Vatic2Darknet.py is a simple Python3 script to convert the [http://carlvondrick.com/vatic/](Vatic annotation tool) format to the [https://pjreddie.com/darknet/yolo/](Darknet YOLO) annotation format for training.

This script checks if your annotation is visible for a certain frame and processes if needed. The output directory contains all the things you need to train Darknet YOLO directly.
"""

import sys
import re
import shutil
import os

class VaticDarknetConvertor(object):
	def __init__(self):
		"""
		The main function for the convertor script.
		"""
		data = ""
		annotations = []

		if(len(sys.argv) <= 4):
			print("USAGE: python vatic2darknet.py path/to/vatic_output.txt img_width img_height output_dir \n Vatic frames must be in the same dir as the vatic_output.txt file!")
			exit(1)
	
		with open(sys.argv[1]) as file:
			data = file.readlines()

		# Clear output directory
		try:
			shutil.rmtree(sys.argv[4])
		except:
			print("{0} doesn't exist, can't clear".format(sys.argv[4]))

		os.mkdir(sys.argv[4])

		annotations = self.parse(data)
		self.convert(annotations)
		self.copy_frames()

	def parse(self, data):
		"""
		Parses the received data from the Vatic annotation text file.
		:param data: A list of strings (lines) from the Vatic annotation file.
		:return: returns the parsed annotations as a list of dictionaries.
		"""
		annotations = []
		for line in data:
			trackId, xMin, yMin, xMax, yMax, frame, lost, occluded, generated, label = line.split(" ")
			label = re.sub(r'^"|"$', '', label) # Remove "..."
			annotations.append({"xmin": float(xMin), "ymin": float(yMin), "xmax": float(xMax), "ymax": float(yMax), "visible": not(bool(int(lost))), "label": label, "frame": int(frame)})

		return annotations

	def convert(self, annotationList):
		"""
		Converts the annotations list in Vatic format to the Darknet YOLO format.

		**Vatic** annotation format documentation:
		
		- [https://github.com/cvondrick/vatic/](https://github.com/cvondrick/vatic/)
		
		- [track ID] [xmin] [ymin] [xmax] [ymax] [frame] [lost] [occluded] [generated] [label] [attributes]
		_([attributes] is optional)_
		
		**Darknet** annotation format documentation:		
		
		- [http://guanghan.info/blog/en/my-works/train-yolo/](http://guanghan.info/blog/en/my-works/train-yolo/)
		
		- [category number] [object center in X] [object center in Y] [object width in X] [object width in Y]

		:param annotationList: A list of dictionaries containing the annotations.
		"""
		imgWidth = float(sys.argv[2])
		imgHeight = float(sys.argv[3])
		labels = []
		tasks = []
		highestFrame = -1

		# Write the annotations to files
		for a in annotationList:
			if(a["visible"] == True): # Darknet ignores the invisible items
				if(a["label"] in labels):
					category = labels.index(a["label"])
				else:
					category = len(labels)
					labels.append(a["label"])
					print("New label ({0}) creating index...".format(a["label"].strip()))
				xCenterNormal = (((a["xmax"] - a["xmin"]) / 2) + a["xmin"]) / imgWidth
				yCenterNormal = (((a["ymax"] - a["ymin"]) / 2) + a["ymin"]) / imgHeight
				widthNormal = (a["xmax"] - a["xmin"]) / imgWidth
				heightNormal = (a["ymax"] - a["ymin"]) / imgHeight
				darknetFormat = "{0} {1} {2} {3} {4}\n".format(category, xCenterNormal, yCenterNormal, widthNormal, heightNormal)
				path = "{0}/{1}.txt".format(sys.argv[4], str(a["frame"]))
				f = open(path, "a")
				f.write(darknetFormat)
				f.close()

				if(highestFrame <= a["frame"]):
					highestFrame = a["frame"]
					pathCurrentFrame = "{0}/{1}.jpg\n".format(sys.argv[4], str(a["frame"]))
					if(pathCurrentFrame not in tasks): # avoid duplicates
						tasks.append(pathCurrentFrame)
	
		# Write the training list to a text file
		pathTasks = "{0}/traininglist.txt".format(sys.argv[4])		
		ft = open(pathTasks, "a")
		ft.writelines(tasks)
		ft.close()

	def copy_frames(self):
		"""
		Copies the Vatic extracted frames to the output directory. The frames must end with the **.jpg** extension!
		"""
		for root, directory, files in os.walk(os.path.dirname(sys.argv[1])):
			for name in files:
				if name.endswith(".jpg"): #filter JPG
					path = os.path.join(root, name)
					shutil.copy(path, "{0}/{1}".format(sys.argv[4], name))


if __name__ == "__main__":
	convertor = VaticDarknetConvertor()
