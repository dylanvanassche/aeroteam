# Things to do

## Tools class
Allerhande handige functies die we nergens kunnen plaatsen.

- Methode om te converteren van relatieve coordinaten naar GPS coordinaten en omgekeerd.
- Methode om een Shapely LineString te extrapoleren.
- Methode om een volgende coordinaat te berekenen op basis van een Shapely LineString (richting) en een Shapely Point (vorig punt)

## Mission class
Superklasse om missies te defineren, bevat een aantal basis functies die elke missie nodig heeft.

- Methode om de drone te starten (arming, default altitude, ...) -> interfacing DroneKit
- Methode om de drone naar een bepaalde GPS coordinaat te sturen -> interfacing DroneKit
- Methode om de drone zijn pakketje te laten droppen -> interfacing DroneKit
- Methode om de drone te laten herpositioneren tot hij het herkende object in het midden van zijn beeld heeft. GPS marker plaatsen van zodra dit succesvol is, zonder dit krijgen we veel afwijkingen.
- Methode waar de missie in uitgevoerd zal worden, deze zal na overerving, overschreven worden. Maak dit abstract?
- Elke actie van de drone en zijn camera beelden moeten op 1 of andere manier zichtbaar worden.

## Darknet class
Klasse die darknetpy encalsuleert, frames leest en zijn resultaten teruggeeft.
- Lees de video stream op 1 of andere manier -> interfacing USB ontvanger
- Schakel CUDA support in, enkel indien beschikbaar (sommige PCs ondersteunen dit niet!)

## Unittests
Super belangrijk! Aangezien we geen drone van Brugge hebben moeten we het weer zelf oplossen... Unittests moeten geschreven worden! Zie [hier](https://docs.python.org/3.5/library/unittest.html) voor meer informatie.

- Tools unittest
- Darknet unittest
- Missions unittests (voor elke missie!)
- GUI unittests

## Python modules
Gebruik zoveel mogelijk kant-en-klare modules, die dingen maken je leven 10x simpelere! Bovendien zijn er vele modules geschreven in C wat de snelheid ten goede komt!
